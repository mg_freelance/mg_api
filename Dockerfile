FROM node:4.6.0
MAINTAINER Lasitha Petthawadu <petthawadu3@gmail.com>
RUN mkdir /api
COPY . /api
WORKDIR  /api
RUN npm install
RUN npm run setup
CMD ["npm", "test"]