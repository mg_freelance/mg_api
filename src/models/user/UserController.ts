// import { Server, Request, IReply } from 'hapi';
// import { hashSync, compareSync } from 'bcrypt';
// import { LoginUser } from '../../mongoose/userModel';
// import { DbControllerBase } from '../DbControllerBase';

// const Joi = require('joi');
// const Boom = require('boom');

// export class UserController extends DbControllerBase {
//     private _server: Server;
//     private static userKeys = [
//         'firstName',
//         'surName',
//         'username',
//         'siteEmail',
//         'roles',
//         'active',
//         'temporaryStaff',
//         'contractStartDate',
//         'contractEndDate',
//         'permissionLevel',
//         'qualifications',
//         'skill',
//         'profileImg',
//         'password',
//         'authToken'
//     ];

//     private responses = {
//         '200': {
//             'description': 'Success'
//         }
//     };

//     constructor(server: Server, trackHistory: any) {
//         super(LoginUser, UserController.userKeys, 'userId', '', 5);
//         this._server = server;
//         this.entitySingular = 'user';
//         this.entityPlural = 'users';
//         this.trackHistory = trackHistory;
//         super.initBaseRoutes();
//     }

//     // private login(request: Request, reply: IReply) {

//     //     const username = request.payload.username.toLowerCase();
//     //     if (!username) {
//     //         reply(Boom.unauthorized('Invalid username or PIN'));
//     //         return;
//     //     }

//     //     LoginUser.findOne({}, (user)=>{
//     //         reply(user);
//     //     })

//         // var loginUser = new LoginUser();
//         // var currentUser = this.entityAssigner.get().filter((col) => {
//         //     return col.username.toLowerCase() == request
//         //         .payload
//         //         .username
//         //         .toLowerCase()
//         // });
//         // if (currentUser == undefined || currentUser.length == 0) {
//         //     reply(Boom.unauthorized('Invalid username or PIN'));
//         //     return;
//         // }

//         // var status = compareSync(request.payload.password, currentUser[0].password);
//         // if (!status) {
//         //     reply(Boom.unauthorized('Invalid username,PIN and site email combination'));
//         //     return;
//         // }
//         // var user = {
//         //     name: currentUser[0].username,
//         //     userId: currentUser[0].userId,
//         //     profileImg: currentUser[0].profileImg,
//         //     authToken: currentUser[0].authToken,
//         //     permissionLevel: currentUser[0].permissionLevel
//         // };
//         // this.trackHistory("Logged In", currentUser[0].authToken);
//         // reply(user);
//   //  }



//     /* Route Definitions */
//     // _____________________________________________________________
//     // LOGIN ROUTE
//     public loginRoute = {
//         method: "POST",
//         path: "/login",
//         config: {
//             auth: false,
//             description: "Endpoint used for login and receiving the authentication token",
//             notes: "This endpoint should be called to authenticate a user and receive the authentica" +
//             "tion token",
//             tags: [
//                 'api', 'v1'
//             ],
//             validate: {
//                 payload: Joi.object({
//                     username: Joi
//                         .string()
//                         .max(30)
//                         .required(),
//                     password: Joi
//                         .string()
//                         .max(30)
//                         .required(),
//                     siteEmail: Joi
//                         .string()
//                         .max(30)
//                         .required()
//                 }),
//                 failAction: (request, reply, source, error) => {
//                     reply(Boom.badRequest('Invalid Payload, username,password and site email expected'));
//                 }
//             },
//             plugins: {
//                 'hapi-swagger': {
//                     responses: this.responses
//                 }
//             }

//         },
//         handler: (request: Request, reply: IReply) => {
//             this.login(request, reply);
//         }

//     };


//     public getRouteList(): any[] {
//         return [this.loginRoute, this.deleteEntityById];
//     }

// }
