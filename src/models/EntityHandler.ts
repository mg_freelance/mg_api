/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */

/**
 * Generic class to handle all the general CRUD operations of a FlowLogic Entity.
 * 
 */

export class EntityHandler {
    private _collection: any;

    /**
     * 
     */
    constructor(collectionFunc: any) {
        this._collection = collectionFunc;
    }

    /**
     * 
     */
    public create(entity: any): void {
        this._collection.get().push(entity);
    }

    /**
     * 
     */
    public update(refId: any, refValue: any, payload: any): boolean {
        for (var elem in this._collection.get()) {
            if (this._collection.get()[elem][refId] == refValue) {
                 var rawElement=this._collection.get()[elem];
                 console.log(rawElement);
                for (var payloadElem in payload) {
                    rawElement[payloadElem] = payload[payloadElem];
                }
                console.log(rawElement);
                return true;
            }
        }
        return false;
    }

    /**
     * 
     */
    public delete(refId: any, refValue: any): Boolean {
        var filtered = this._collection.get().filter((col) => {
            return col[refId] != refValue;
        });
        if (filtered.length < this._collection.get().length) {
            this._collection.set(filtered);
            return true;
        } else {
            return false;
        }
    }


    /**
     * 
     */
    public getList(): any {
        return this._collection.get();
    }

    /**
     * 
     */
    public get(refId: any, refValue: any): any {
        var filtered = this._collection.get().filter((col) => {
            return col[refId] == refValue;
        });
        return filtered;
    }
}