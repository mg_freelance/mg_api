import { Server, Request, IReply } from 'hapi';
const Boom = require('boom');
import { hashSync, compareSync } from 'bcrypt';
import { ControllerBase } from '../ControllerBase';
import { JobSetupEditCriteriaSampleData, FieldOptions } from "../../data/jobsetup/JobSetupEditCriteriaSampleData";
import { JobsTestData } from "../../data/jobsetup/JobsTestData";
/**
 * 
 */
export class JobSetupController extends ControllerBase {
    private _server: Server;
    private readonly VIEWS: string[] = ["PLANNING", "SCHEDULE", "SEQUENCE", "PRODUCTION", "MURPHY", "COMPLETE"];

    /**
     * 
     */
    constructor(server: Server, trackHistory: any) {
        super(null, null, 'jobsetup', 'jobsetupid', 1);
    }
    private getAllEditSetupInfo = {
        method: "GET",
        path: "/worklist/edit",
        config: {
            auth: false,
            description: "Endpoint used to get saved edit information of all views.",
            notes: "Endpoint used to get all edit info",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            reply(JobSetupEditCriteriaSampleData.getInstance().getAllEditCriteriasByUserId("U1001"));
        }

    };

    private getEditSetupInfo = {
        method: "GET",
        path: "/worklist/edit/{view}",
        config: {
            auth: false,
            description: "Endpoint used to get saved edit information for a perticulat view.",
            notes: "The view parameter must be of PLANNING | SCHEDULE | SEQUENCE | PRODUCTION | MURPHY | COMPLETE " + 
            "Parameter is case insensitive",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            let ERROR_NO_USERID = "User Id not found";

            let view = request.paramsArray[0];
            let userId = request.headers['user-id'];
            if (userId === undefined) {
                reply(Boom.badRequest(ERROR_NO_USERID));
            } else {
                reply(JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, view));
            }
            
        }
    };

    private updateEditSetupInfo = {
        method: "POST",
        path: "/worklist/edit/{view}",
        config: {
            auth: false,
            description: "Endpoint used to update edit setup information for a perticulat view.",
            notes: "The view parameter must be of PLANNING | SCHEDULE | SEQUENCE | PRODUCTION | MURPHY | COMPLETE " + 
            "Parameter is case insensitive. Should include HEADER entry <USER-ID, 'userid'> ",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            reply(this.updateEditSetupInformation(request));
        }
    };

    private getAllSetupViewData = {
        method: "GET",
        path: "/worklist/view",
        config: {
            auth: false,
            description: "Endpoint used to get data for all views.",
            notes: "The result of the API is based on previous filter criterias saved from the edit view for each view",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            let userId = request.headers['user-id'];
            let ERROR_NO_USERID = "User Id not found";
            if (userId === undefined) {
                reply(Boom.badRequest(ERROR_NO_USERID));
            } else {
                reply(new JobsTestData().getAll(userId));
            }
            
        }
    }

    private getSetupViewData = {
        method: "GET",
        // path: "/worklist/view/{view}",
        path: "/jobs/{view}",
        config: {
            auth: false,
            description: "Endpoint used to get data for a perticulat view.",
            notes: "The view parameter must be of PLANNING | SCHEDULE | SEQUENCE | PRODUCTION | MURPHY | COMPLETE " + 
            "Parameter is case insensitive. The result of the API is based on previous filter criterias saved from the edit view",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            this.getViewLayoutData(request).then(data => {
                reply(data);
            });
        }
    }

    private updateEditSetupInformation(request: Request): any {
            let view = request.paramsArray[0];
            let payload = request.payload;
            let userId = request.headers['user-id'];
            
            let ERROR_INVALID_VIEW = "Could not find matching view " + view;
            let ERROR_NO_USERID = "User Id not found";
            let ERROR_INVALID_PAYLOAD= "Payload should be an array";
            let ERROR_UPDATE_FAILED= "Failed to update record. User with user-id: " + userId + " not found";
            let SUCCESS = "Updated Sucessfully";

            if (userId === undefined) {
                return Boom.badRequest(ERROR_NO_USERID);
            } else if (this.VIEWS.indexOf(view.toUpperCase()) < 0 ) {
                return Boom.notAcceptable(ERROR_INVALID_VIEW);
            } else if (!Array.isArray(payload)) {
                return Boom.badData(ERROR_INVALID_PAYLOAD);
            } else {
                let result: boolean = JobSetupEditCriteriaSampleData.getInstance().updateEditSetupInformation(userId, view, payload);
                if(result) { 
                    return ({status: 200, message: SUCCESS});
                } else {
                    return Boom.badData(ERROR_UPDATE_FAILED);
                } 
            }

    }
    private getViewLayoutData(request: Request) {
        let view = request.paramsArray[0];
        let userId = request.headers['user-id'];
        let data: JobsTestData = new JobsTestData();
        return data.getDataForView(view, userId);
        /*switch (view.toLocaleUpperCase()) {
            case this.VIEWS[0]:
                return data.getPlanningViewTestData();
            case this.VIEWS[1]:
                return data.getScheduleViewTestData();
            case this.VIEWS[2]:
                return data.getSequenceViewTestData();
            case this.VIEWS[3]:
                return data.getProductionViewTestData();
            case this.VIEWS[4]:
                return data.getMurphyViewData();
            case this.VIEWS[5]:
                return data.getCompletedViewData();
        }*/

    }

    /**
    * Returns all routes defined in class.
    */
    public getRouteList(): any[] {
        return [this.getAllEditSetupInfo, this.getEditSetupInfo, this.updateEditSetupInfo, 
        this.getAllSetupViewData, this.getSetupViewData];
    }

}