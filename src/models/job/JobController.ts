/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Chandima Anuradha
 */
import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';

import { DbControllerBase } from '../DbControllerBase';
import { Job } from '../../mongoose/jobsModel'
import { UserAllocation } from '../../mongoose/userAllocationModel'
import { LoginUser } from '../../mongoose/userModel'
const Boom = require('boom');

export class JobController extends DbControllerBase {
    private _server: Server;

    private static departmentKeys = [
        'jobID',
        'createDate', 
        'estimatedCompletionDate',
        'jobInfo', 
        'notes', 
        'jobEvents', 
        'workflowID',
        'jobLineList', 
        'order', 
        'customList', 
        'completedCheklistItems', 
        'jobProgress', 
        'attachments', 
        'murphy', 
        'sections'
        
    ];


    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    constructor(server: Server, trackHistory: any, nextIdIncrement: number) {
        super(Job , JobController.departmentKeys, 'jobID', 'JB00', nextIdIncrement);

        this._server = server;
        this.entitySingular = 'job';
        this.entityPlural = 'jobs';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();
        
    }

    toCamelCase(str) {
        return str.replace(/^([A-Z])|\s(\w)/g, function (match, p1, p2, offset) {
            if (p2) return p2.toUpperCase();
            return p1.toLowerCase();
        });
    }
    
    /**
     * 
     */
    private getAllJobs = {
        method: "GET",
        path: "/jobs",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {

                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }

                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    
    /*--------------------------Start Jobs in planning----------------------------------------------------------------------*/
    private getPlanningJobs = {
        method: "GET",
        path: "/jobs/planning",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {

                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }

                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    /*--------------------------End Jobs in planning----------------------------------------------------------------------*/
    
    /*--------------------------Start Jobs in schedule----------------------------------------------------------------------*/
    private getScheduleJobs = {
        method: "GET",
        path: "/jobs/schedule",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {
                            
                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }
                                if(evt['Scheduled']!="")
                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                            
                            
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    /*--------------------------End Jobs in schedule----------------------------------------------------------------------*/

    /*--------------------------Start Jobs in sequence----------------------------------------------------------------------*/
    private getSequenceJobs = {
        method: "GET",
        path: "/jobs/sequence",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {

                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }

                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    /*--------------------------End Jobs in sequence----------------------------------------------------------------------*/

/*--------------------------Start Jobs in production----------------------------------------------------------------------*/
    private getProductionJobs = {
        method: "GET",
        path: "/jobs/production",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {

                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }

                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    /*--------------------------End Jobs in production----------------------------------------------------------------------*/
    
    /*--------------------------Start Jobs in murphy----------------------------------------------------------------------*/
    private getMurphyJobs = {
        method: "GET",
        path: "/jobs/murphy",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {

                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }

                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    /*--------------------------End Jobs in murphy----------------------------------------------------------------------*/
    
    /*--------------------------Start Jobs in complete----------------------------------------------------------------------*/
    private getCompleteJobs = {
        method: "GET",
        path: "/jobs/complete",
        config: {
            auth: false,
            description: "Endpoint used to get all jobs",
            notes: "Endpoint used to get a all Jobs",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            
            Job.find({}, (err, job) => {
                if (err) { reply(Boom.notFound('Couldnt find jobs'));}
                else {
                        var filteredList = [];
                    
                        for (var elem in job) {

                            let newJob ={}
                            job[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    newJob['jobID'] = job.jobID
                                }else{
                                    newJob[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })

                             job[elem]['jobEvents'].forEach((evt)=>{
                                let suffix = ''
                                if(evt.EventName == 'Starting Job'){
                                    suffix = 'StartDate'
                                }else{
                                    suffix = 'EndDate'
                                }

                                newJob[this.toCamelCase('Scheduled') + suffix] = evt['Scheduled']
                                newJob[this.toCamelCase('inSequence') + suffix] = evt['inSequence']
                                newJob[this.toCamelCase('inProduction') + suffix] = evt['inProduction']
                                newJob[this.toCamelCase('inCompletion') + suffix] = evt['inCompletion']
                                
                            })
                                newJob['jobID'] =  job[elem].jobID
                                newJob['workflowID'] =  job[elem].workflowID
                                newJob['jobCreateDate'] =  job[elem].createDate
                                newJob['estimatedCompletionDate'] = job[elem].estimatedCompletionDate
                                newJob['workflowFinishDate'] = job[elem].jobProgress.workflowFinishDate
                                newJob['currentDept'] = job[elem].jobProgress.currentDepartment[0]
                                newJob['currentUser'] = job[elem].jobProgress.currentDepartment[3]
                                newJob['murphyType'] = job[elem].murphy.currentState[0]
                                newJob['murphyTimestamp'] = job[elem].murphy.currentState[1]
                                newJob['murphyReason'] = job[elem].murphy.currentState[2]
                                newJob['sections'] = job[elem].sections                    
                            
                            
                            filteredList.push(newJob);
                        }
                    
                    reply(filteredList);
                }
            })
        }
    }
    /*--------------------------End Jobs in complete----------------------------------------------------------------------*/

    /*--------------------------Start user assingned job section----------------------------------------------------------------------*/

    private getAssignedUser = {
        method: "GET",
        path: "/jobs/getJobSectionUser/{jobID}/{sectionID}",
        config: {
            auth: false,
            description: "Endpoint used to user of an assigned Sections of a job",
            notes: "Endpoint used to user of an assigned Sections of a job",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {

            UserAllocation.findOne({
                jobID:  request.params["jobID"],
                sectionId: request.params["sectionID"]
            }, (err, allocation)=>{
                 if (err) { reply(Boom.notFound('Something went wrong in user allocation'));}
                 else{
                     if(allocation){
                         LoginUser.findOne({
                             userId: allocation.userId
                         }, (err, user)=>{
                             reply(user.username)
                         })
                     }else{
                         reply('')
                     }
                 }
            })
        }
    }

    /*--------------------------End user assingned job section----------------------------------------------------------------------*/


   

    /**
     * 
     */
    public getRouteList(): any[] {
        // return [this.getAllJobs,this.getPlanningJobs,this.getScheduleJobs,this.getSequenceJobs,this.getProductionJobs,this.getMurphyJobs,this.getCompleteJobs, this.getEntityByIdRoute, this.addEntity, this.updateEntity, this.deleteEntityById];
         return [this.getAllJobs,this.getEntityByIdRoute, this.addEntity, this.updateEntity, this.deleteEntityById, this.getAssignedUser];
    }
}