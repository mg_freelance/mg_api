
import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
import { Checklist } from '../../mongoose/checklistModel'
import { DbControllerBase } from '../DbControllerBase';

export class ChecklistController extends DbControllerBase {
    private _server: Server;
    private static checklistKeys = [
        'checkListId',
        'checkListName',
        'items',
        'checkListOrder'
    ];

    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    constructor(server: Server, trackHistory: any, nextIdIncrement: number) {
        super(Checklist, ChecklistController.checklistKeys, 'checkListId', 'CHK00', nextIdIncrement);
        this._server = server;
        this.entitySingular = 'checklist';
        this.entityPlural = 'checklists';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();
    }

    private getAllChecklists = {
        method: "GET",
        path: "/checklists",
        config: {
            auth: false,
            description: "Endpoint used to get all checklists",
            notes: "Endpoint used to get a all checklists",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            return Checklist.find({})
            .then((modelInstances) => {
                //response["skill"] = modelInstances;
                let response = []
                modelInstances.forEach((model)=>{
                    response.push({
                        checkListId: model.checkListId,
                        checkListName: model.checkListName
                    })
                })
                reply(response)
            });
        }
 
    }

    /**
     * 
     */
    public getRouteList(): any[] {
        return [this.getAllChecklists, this.getEntityByIdRoute, this.addEntity, this.updateEntity, this.deleteEntityById];
    }
}

