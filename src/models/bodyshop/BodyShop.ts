/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */
import {IEntity} from '../IEntity';
export class BodyShop implements IEntity {

    private _bodyShopId : String;
    private _bodyShopName : String;
    private _bodyShopDescription : String;
    private _bodyShopLocation : String;

    /**
     * Constructs a bodyshop entity
     */
    public constructor(bodyShopName : String, bodyShopDescription : String, bodyShopLocation : String) {
        this._bodyShopName = bodyShopName;
        this._bodyShopDescription = bodyShopDescription;
        this._bodyShopLocation = bodyShopLocation;
    }

    /**
     * Returns the body shop name.
     */
    get bodyShopName() : String {return this._bodyShopName;}

    /**
     * Returns the body shop description.
     */
    get bodyShopDescription() : String {return this._bodyShopDescription;}
    
    /**
     * Returns the body shop location.
     */
    get bodyShopLocation() : String {return this._bodyShopLocation;}


    getId():any{
    }

}