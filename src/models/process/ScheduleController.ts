import { JobsTestData } from "../../data/jobsetup/JobsTestData";
import { Job } from '../../mongoose/jobsModel'
import { CalendarPattern } from '../../mongoose/patternModel'
import{ SequencingController }from "./SequncingController"

const moment = require('moment');



export class ScheduleController {

    
    public getRecommendedStartDate(userId){
        let data: JobsTestData = new JobsTestData();
        let jobsInProduction = data.getDataForView('production', userId)
        let jobsInPlanning = data.getDataForView('planning', userId)
        let jobsInSequence = data.getDataForView('sequence', userId)

        let totalHoursInSystem = 0
        totalHoursInSystem += this.calculateHrs(jobsInProduction)
        totalHoursInSystem += (this.calculateHrs(jobsInPlanning) + this.calculateHrs(jobsInSequence))

        let expansionFactor = 1

        totalHoursInSystem  = totalHoursInSystem  * expansionFactor;

        let recommendedDate = new Date()
        let techHours = ScheduleController.getTotalTechniciansHours(recommendedDate.toISOString());

        let totalTempHours= totalHoursInSystem

        while(techHours<totalTempHours   ){
            recommendedDate = new Date(recommendedDate.setDate(recommendedDate.getDate() + 1 ));

            totalTempHours   = totalTempHours  - techHours ;
        }

        return recommendedDate ;
    }


    /**
     * calculates total Hours needed for a set of jobs with workflows present
     * @param jobData is the set of jobs objects with at least jobIDs present
     * @returns total Hours needed for the  set of jobs to complete in hours
     */
    private calculateHrs(jobData){
        let jobs = []
        Job.find({}).then((resp)=>{
            jobs = resp
        })
        let Hrs = 0

        for( let elem in jobData){
            for(let elem2 in jobs){
                if(jobData[elem]['jobID'] == jobs[elem2]['jobID']){
                    if(jobs[elem2].workflowID != ''){
                        if(jobs[elem2].jobProgress.progress[0] == null){
                            /**Job hasn't been started*/                    
                            for(let section in jobs[elem2].sections){
                                Hrs += parseFloat(jobs[elem2].sections[section][2])
                            }
                        }else if(jobs[elem2].jobProgress.currentSectionId == ''){
                            /** section is complete but the next section is not started yet */

                            let completedDepartments = jobs[elem2].jobProgress.progress.length
                            for(let key in jobs[elem2].sections){

                                /**including only the uncompleted section times */
                                if(jobs[elem2].sections[key][3] <= completedDepartments){
                                    completedDepartments -= parseInt(jobs[elem2].sections[key][3])
                                }else{
                                    Hrs += parseFloat(jobs[elem2].sections[key][2])
                                }
                            }
                        }else{
                            /** A section is in progress */
                            let nextSection = parseInt(jobs[elem2].jobProgress.currentSectionId) + 1

                            /** getting the remaining Hours in the ongoing section
                             *      remaining hrs = ongoingSectionTotalHrs - currentSectionLoggedTime
                             *  this can be negative
                             */
                            let currentSectionRemainingHrs = parseInt(jobs[elem2].sections[nextSection-1][2]) - 
                                    ((( new Date().getTime() - Date.parse(jobs[elem2].sections[nextSection-1][5]) ) / (1000*60*60*24))* SequencingController.hoursPerDay)

                            let murphyHrs = 0 

                            
                            /** If there's an ongoing murphy delay in the section */
                            if(jobs[elem2].murphy.currentState[0] != '' && jobs[elem2].murphy.Delay[0] != null){
                                for(let murphyJob in jobs[elem2].murphy.Delay){
                                    
                                    /** only add the murphys whith start time >= section startTime */
                                    if(Date.parse(jobs[elem2].murphy.Delay[murphyJob].addedDate) >= Date.parse(jobs[elem2].sections[nextSection-1][5])){ 
                                            
                                        if(jobs[elem2].murphy.Delay[murphyJob].releaseDate != ''){

                                            murphyHrs += (( Date.parse(jobs[elem2].murphy.Delay[murphyJob].releaseDate) - 
                                                                Date.parse(jobs[elem2].murphy.Delay[murphyJob].addedDate)) / (1000*60*60*24))*SequencingController.hoursPerDay

                                        }else{
                                        
                                            murphyHrs += ((new Date().getTime() - Date.parse(jobs[elem2].murphy.currentState[1])) / (1000*60*60*24))*SequencingController.hoursPerDay 
                                        }
                                
                                    }                               

                                }                       
                            }else if(jobs[elem2].murphy.currentState[0] == '' && jobs[elem2].murphy.Delay[0] != null){                                
                                /** If there's no ongoing murphy but had been in murphy delay */  

                                        for(let murphyJob in jobs[elem2].murphy.Delay){
                                            if(Date.parse(jobs[elem2].murphy.Delay[murphyJob].addedDate) >= Date.parse(jobs[elem2].sections[nextSection-1][5])){

                                                murphyHrs += (( Date.parse(jobs[elem2].murphy.Delay[murphyJob].releaseDate) - 
                                                                    Date.parse(jobs[elem2].murphy.Delay[murphyJob].addedDate)) / (1000*60*60*24))*SequencingController.hoursPerDay 

                                            }
                                        }
                            }

                            if(murphyHrs + currentSectionRemainingHrs > 0){
                                /**time logged hasn't exceeded the planned no of hours for the section */
                                Hrs += (currentSectionRemainingHrs + murphyHrs)
                            }else{
                                Hrs += parseFloat(jobs[elem2].sections[nextSection-1][2])
                            }

                            /** addition of hrs of the  remaining sections after the current section */
                            for( ; nextSection <= Object.keys(jobs[elem2].sections).length; nextSection++){
                                Hrs += parseFloat(jobs[elem2].sections[nextSection][2])
                            }
                        }
                    }
                }   
            }    
        }
        return Hrs
    }

    /**
     * Sums technician hours for a given day
     * @param date date to get calculate hours. date should be in ISO format. eg: 2017-03-06T09:48:00.000Z
     * @returns total technician time in hours
     */
    private static getTotalTechniciansHours(date: string): number {
        const formatedDate = moment(new Date(date)).format("YYYY/MM/DD");
        const roleIdTechnician = "R001";
        let calendarData = [];
        CalendarPattern.find({}).then((resp)=>{
            calendarData = resp
        });
        let totalTimeinMS = 0;
        for(const patternObj of calendarData) {
            patternObj.rolePattern.map((assignment) => {
                if((assignment.date === formatedDate) && (assignment.roleId === "R001")) {
                    let startTime = moment((formatedDate + ' ' + assignment.startTime),"DD/MM/YYYY HH.mm");
                    let endTime = moment((formatedDate + ' ' + assignment.endTime),"DD/MM/YYYY HH.mm");
                    let diffInMS = endTime.diff(startTime);
                    totalTimeinMS += diffInMS;
                }
            })
        }

        return (totalTimeinMS/(60 * 60 * 1000))
    }
}