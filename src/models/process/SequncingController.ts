import { Data } from "../../data/Data"
import { UserAllocation } from '../../mongoose/userAllocationModel'
import { LoginUser } from '../../mongoose/userModel'
import { Job } from '../../mongoose/jobsModel'
import { Workflow } from '../../mongoose/workflowModel'
import { CalendarFilter }  from '../../api-server/calendar-roles/CalendarFilter';
import { CalendarPattern }  from  '../../mongoose/patternModel';
import { JobsTestData } from "../../data/jobsetup/JobsTestData";

const moment = require('moment');



export class SequencingController {

    public static hoursPerDay = 8;

    public static getEndDate(startDate: string, hoursToAdd: number){

        let start_Date = new Date(startDate);
        let  endDate = new Date(startDate);
        let noOfDaysToAdd = Math.ceil(hoursToAdd*(SequencingController.hoursPerDay/24));
        let count = 0;
        while(count < noOfDaysToAdd){
            endDate = new Date(start_Date.setDate(start_Date.getDate() + 1));
            if(endDate.getDay() != 0 && endDate.getDay() != 6){
            count++;
            }
        }
        return endDate
    }

    /**
     * Method used to trigger the sequence logic to make job allocations
     */
    public TriggerSequenceLogic(sequenceOrder? : Object[]){

        this.clearPendingAllocations()
        this.createTechFreePoints()
        this.setAllJobsWithoutTechs(sequenceOrder)
        
        let jobsNeedingTechs = Data.pendingJobs
        let skipJobfromAllocating = ''
        for(let job in jobsNeedingTechs){
            if(skipJobfromAllocating == job['jobID']){  // if a previous section of the job is not allocated skips the next sections
                continue;
            }
            let sectionId = job['section'][7]
            let plannedHrs = job['section'][2]
            let sectionSkills = job['section'][1]

            let techFreePoints = Data.techFreeTime
            let isAllocated = false
            for(let techFree in techFreePoints){
                let skills: string[] = JSON.parse(JSON.stringify(sectionSkills))

                techFreePoints[techFree]['skill'].forEach((skill)=>{
                    if(skills[0] != null){
                       skills =  skills.filter((sectionSkill)=>{
                            return sectionSkill != skill
                        })
                    }
                })

                if(skills[0] == null && plannedHrs == techFreePoints[techFree]['timeRemaning']){    
                    
                    this.allocateJob({
                        userId: techFreePoints[techFree]['userId'],
                        jobID:  job['jobID'],
                        sectionId: sectionId,
                        jobStatus: this.getNextJobStatus(techFreePoints[techFree]['userId']), 
                        startedTime: '',
                        completedTime: '',
                        techFreeTime: techFree['techFreeTime']
                    })
                    isAllocated = true
                    Data.techFreeTime.splice(parseInt(techFree), 1)
                    break;
                    
                }else if(skills[0] != null && skills.length < sectionSkills.length){

                    let isFloatersAvailable = this.isAvailableFloatersWithSkill(skills, job['workflowID'],  techFreePoints[techFree].techFreeTime)
                    
                    if(isFloatersAvailable){
                         this.allocateJob({
                            userId: techFreePoints[techFree]['userId'],
                            jobID:  job['jobID'],
                            sectionId: sectionId,
                            jobStatus: this.getNextJobStatus(techFreePoints[techFree]['userId']), 
                            startedTime: '',
                            completedTime: '',
                            techFreeTime: techFree['techFreeTime']
                        })
                        isAllocated = true
                        Data.techFreeTime.splice(parseInt(techFree), 1)
                        break;
                    }
                }
            }
            if(!isAllocated){
                skipJobfromAllocating = job['jobID']
            }
        }
    }
    
    /**
     * 
     * Method used to clear job allocations which are not yet started
     */
    private clearPendingAllocations(){
        let allocations = []
        let filter = {}
        filter['startedTime'] = ''

        UserAllocation.remove(filter)
        
        // let newAllocations = []
        // UserAllocation.find({}).then((newalloc)=>{
        //     newAllocations = newalloc
        // })

        // Data.userAllocation = newAllocations    ///USER allocation saving TOBE  ADDED
    }
    
    /**
    * //TOBE RECONFIGURED TO PUSH TO THE ALLOCATION TABLE ON THE DATABASE
    *Method uset to send a new allocation to the database
    *@param jobObject the new job object in the form of an object in the allocations table
    */
    private allocateJob(jobObject){     
       
        UserAllocation.create(jobObject)
    }

    /**
    *returns the next count for the jobStatus of a perticular user
    *@param userId of the user
    */
    private getNextJobStatus(userID: string){
        let allocatedData = []
       UserAllocation.find({}).then((resp)=>{
           allocatedData = resp
       })

        let filteredAllocations = allocatedData.filter((allocation)=>{
            return allocation.userId == userID && parseInt(allocation.jobStatus) < 1000
        })

        if(filteredAllocations[0] != null){
            let currentStats = filteredAllocations.map((allocation)=>{
                return allocation.jobStatus
            })

            currentStats.sort()

            return parseInt(currentStats[currentStats.length-1]) + 1

        }else{       //no jobs assigned for the user
            return 0 
        }
    }

    /**
    *returns true if the necessary skills are present in the set of floates of the respective departments of the workflow
    *@param skills necessary remaining skills for the section
    *@param workflowId of the workflow of the job
    *@param date of which the considering technician is free for the job
    */
    private isAvailableFloatersWithSkill(skills: string[], workflowID: string, date: string){
        let relatedDepartments = this.getRelatedDepartments(workflowID, skills)
        let FloatersinDepartments = []

        LoginUser.find({}).then((userData)=>{
            let neededSkillSet = skills

            relatedDepartments.forEach((department)=>{
                FloatersinDepartments.concat(
                    SequencingController.getAllUsers('R004', date, department)
                )
            })

        //Uncomment after adding the global department
        /*
        *    FloatersinDepartments.concat(
        *        SequencingController.getAllUsers('R004', date, 'Dept000')
        *    )
        */
            FloatersinDepartments.forEach((floaterUser)=>{
                if(neededSkillSet[0] != null){
                    let UserSkillSet: Object[] = userData.filter((user)=>{
                        user.userId == floaterUser.userId
                    })[0]['skill']
                    
                    UserSkillSet.forEach((userSkill)=>{
                        neededSkillSet = neededSkillSet.filter((skill)=>{
                            return skill != userSkill['skillId']
                        })
                    })
                }
            })

            return neededSkillSet[0] == null? true: false     
        })
           

    }

    /**
    *returns the list of departments in a workflow that are related to the given skill set of a section in the workflow
    *@param workflowId if the workflow
    *@param skills - list of skills in the necessary section of the workflow in the form ['SK001', 'SK002']
    */
    private getRelatedDepartments(workflowId: string, skills: string[]){
        let workflowData = {}
        let filter = {}
        filter['workflowId'] = workflowId;
        Workflow.find(filter).then((workflow)=>{
            workflowData = workflow
        })

        let departmentList = []
        let skillDeptMap: Object[] = workflowData[0]['SkillDept']

        skills.forEach((skill)=>{
            let skillMap = skillDeptMap.filter((mapping)=>{
                return skill == mapping['skillId']
            })
            departmentList.push(skillMap[0]['departmentId'])
        })
        
        return departmentList
    }

    /**
    *recreates the pendingJobObject list by sorting the jobs in production and jobs in sequence
    *@param (optional) a new order for all/some of the jobs in sequence list
    *@returns list of pendingJobObjects
    */
    private setAllJobsWithoutTechs(sequenceOrder? : Object[]){
        Data.pendingJobs = []
        let data: JobsTestData = new JobsTestData();
        data.getDataForView('production', '').then((jobsInProduction)=>{
            data.getDataForView('sequence', '').then((jobsInSequence)=>{
                Job.find({}).then((jobData)=>{
                    let jobsInProductionData = []
                    let jobsInSequenceData = []

                    jobsInProduction.forEach((productionJob)=>{
                        jobsInProductionData.push(
                            jobData.filter((job)=>{
                                return job.jobID == productionJob.jobID
                            })[0]
                        )
                    })

                    jobsInSequence.forEach((sequenceJob)=>{
                        jobsInSequenceData.push(
                            jobData.filter((job)=>{
                                return job.jobID == sequenceJob.jobID
                            })[0]
                        )
                    })

                    jobsInProductionData.sort((job_1, job_2)=>{
                        return moment(new Date(job_1['estimatedCompletionDate'])) - moment(new Date(job_2['estimatedCompletionDate']))
                    })

                    jobsInSequenceData.sort((job_1, job_2)=>{
                        return moment(new Date(job_1['jobEvents'][0].inSequence)) - moment(new Date(job_2['jobEvents'][0].inSequence))
                    })

                    if(sequenceOrder != null){
                        let order = []
                        let orderData = []
                        sequenceOrder.forEach((job)=>{
                            let index = 0
                            let found = false
                            jobsInSequenceData.forEach((jobinSequence)=>{
                                if(jobinSequence.jobID != job && !found){
                                index++ 
                                }else if(jobinSequence.jobID == job){
                                    found = true
                                }
                            })

                            order.push(index)
                            orderData.push(jobsInSequenceData[index])
                        })

                        order.sort()
                        order.forEach((index)=>{
                            jobsInSequenceData[index] = orderData.shift()
                        })
                    }

                    //let allPendingJobs = jobsInProductionData.concat(jobsInSequenceData)

                    let newPendingJobList = []

                    jobsInProductionData.forEach((job)=>{
                        newPendingJobList.concat(this.createPendingJobObject(job, '1'))
                    })

                    jobsInSequenceData.forEach((job)=>{
                        newPendingJobList.concat(this.createPendingJobObject(job, '0'))
                    })

                    Data.pendingJobs = newPendingJobList        // check if it can be saved
                })        
            })
        })    

      
       
    }

    /**
    *creates new pendingJob objects by splitting a job object into it's todo sections in the workflow
    *@returns list of pendingJobObjects
    */
    private createPendingJobObject(job: Object, isProduction: String){
        let sections = this.splitByNextSections(job)

        let PendingjobsfromSections= []
        sections.forEach((section)=>{
            let newObject = {
                jobID: job['jobID'],
                isProduction : isProduction,
                section : section,
                workflowID: job['workflowID'],
                isAllocated:'0',
                ScheduledStartDate : isProduction == '1' ? '': job['jobEvents'][0].inSequence,
                plannedCompletedDate : job['estimatedCompletionDate'],
                isStarted:'0'
            }
            PendingjobsfromSections.push(newObject)
        })

        return PendingjobsfromSections        
    }
    
    /**
     *Splits a job by it's to be completed sections
     *@returns list of to be completed sections
     */
    private splitByNextSections(job: Object){
        let sections = []
        job['sections'].forEach((section)=>{
            if(section[5] != ''){  // if no start Date, section not started
                sections.push(section)
            }
        })
        return sections
    }

    /**
     *creates new techFreePoints for all users
     */
    private createTechFreePoints(){
        Data.techFreeTime = []
        let userStatus = {};
        let users = SequencingController.getAllUsers('R001', new Date().toISOString() ); // technitians
        users.concat(SequencingController.getAllUsers('R003', new Date().toISOString())); // painters
        let newTechFreePoints = []

        users.forEach((user) => {

            let tempTechFreeTime = new Date();
            userStatus = SequencingController.getUserStatus(user.UserId);

            if(userStatus['calendaredOff']){
                tempTechFreeTime = new Date(SequencingController.NextStartTimeAsTech(user.UserId, tempTechFreeTime.toISOString()));                
            }
            
            let techFreeDateTime = tempTechFreeTime

            let jobWithTechAssigned = SequencingController.getUserCurrentAllocationDetails(user.userId);

            if(jobWithTechAssigned != null){
                switch(userStatus['state']) {       
                    case "PRODUCTIVE ":
                        techFreeDateTime =  SequencingController.plannedSectionFinishTime(jobWithTechAssigned.jobID, jobWithTechAssigned.sectionId, tempTechFreeTime.toISOString(), userStatus)                     
                        break;
                    case "TECHONHOLD":                
                        techFreeDateTime =  SequencingController.plannedSectionFinishTime(jobWithTechAssigned.jobID, jobWithTechAssigned.sectionId, tempTechFreeTime.toISOString(), userStatus)                     
                        break;
                    default:
                        break;
                }
            }

            newTechFreePoints.push(this.addTechFreePoints(user.userId, techFreeDateTime.toISOString()))

        })
        
        Data.techFreeTime = newTechFreePoints

    }

    /**
     * Returns all users assigned for a role on a given date and for a given department(optional)
     * @param roleId string. Role ID to match for.
     * @param date Should be in ISO format. eg: 2017-03-06T09:48:00.000Z
     * @param departmentId (optional) to obtain all users in a department with a perticular skill
     * @returns list of users. List element will be in the format of {userId: 'userId', userName: 'userName'}
     */
    private static getAllUsers(roleId: string, date: string, departmentId?: string): any[] {
        const formatedDate = moment(new Date(date)).format("YYYY/MM/DD");
        let calendarData = {}
        CalendarPattern.find({}).then((patterns)=>{
            calendarData = patterns
        })
        let users = [];
        for(const patternObj of CalendarFilter.getCalendarPattern()) {
            patternObj.rolePattern.map((assignment) => {
                if((assignment.date === formatedDate) && (assignment.roleId === roleId) && (departmentId? assignment.department === departmentId: true)) {
                    users.push({
                        userId: patternObj.userId,
                        userName: patternObj.userName
                    })
                }
            })
        }

        return users;
    }

     /**
     * Returns the current user state of the user
     * @param userId string. User ID to match for.
     * 
     */
    private static getUserStatus(userId: string){
         let userStateList = []
         LoginUser.find({}).then((users)=>{
             userStateList = users
         })
         var filtered = userStateList.filter((state) => {
            return state.userId.toString() === userId;
        });

        return filtered[0]
    }


     /**
     * Returns the allocation detailse of the current job assigned to the given user
     * @param userId string. User ID to match for.
     * 
     */
    private static getUserCurrentAllocationDetails(userId){
        let users = []
        LoginUser.find({}).then((usrs)=>{
            users = usrs
        })
        let currentUserAllocations = users.filter((allocation)=>{
            return (allocation['userId'] == userId && parseInt(allocation['jobStatus']) == 0)
        })

        return currentUserAllocations[0]
        
    }

    /**
     * Returns the next date that the user is free
     * @param jobId string. jobId of the current working job.
     * @param sectionId string. current working section of the job.
     * @param sectionId string. current working section of the job.
     * @param date Should be in ISO format. eg: 2017-03-06T09:48:00.000Z
     * @param currentUserStatus Object. user state object of the current user
     */
    private static plannedSectionFinishTime(jobId: string, sectionId: string,  date: string, currentUserStatus: Object){
        let currentJob = []
       Job.find({ 'jobID': jobId })((job)=>{
            currentJob = job
        })
        let sectionRemainingTime = 0
        if(currentUserStatus['state'] == 'TECHONHOLD'){
            sectionRemainingTime = parseFloat(currentJob['sections'][sectionId][2]) - 
                ((( Date.parse(currentUserStatus['timestamp']) - Date.parse(currentJob['sections'][sectionId][5]) ) / (1000*60*60*24))*SequencingController.hoursPerDay)
        }else if(currentUserStatus['state'] == 'PRODUCTIVE'){
            sectionRemainingTime = parseFloat(currentJob['sections'][sectionId][2]) - 
                ((( new Date().getTime() - Date.parse(currentJob['sections'][sectionId][5]) ) / (1000*60*60*24))* SequencingController.hoursPerDay)
        }

        return SequencingController.getEndDate(date, sectionRemainingTime)
    }

     /**
     * Check for the next time slot assigned as technician from given time.
     * @param userId Use'r ID 
     * @param date specific date to filter records
     * @param time optional. if provided will look for the next specific assignment from given time. otherwise use current time.
     * @returns null if no assignment found on the day. Else will return the start timeof next job in the format of hh.mm
     */
    private static NextStartTimeAsTech(userId: string, date: string, time?: string): string {
        let timeNow = time ? time: (new moment().format("HH.mm")); 
        const formatedDate = moment(new Date(date)).format("YYYY/MM/DD");
        const roleIdTechnician = "R001";

        let __now = moment((formatedDate + ' ' + timeNow),"DD/MM/YYYY HH.mm");
        let __nextAssignment = moment((formatedDate + ' ' + timeNow),"DD/MM/YYYY HH.mm");
        let result = null;


        for(const patternObj of  CalendarFilter.getCalendarPattern()) {
            if(patternObj.userId != userId) {
                continue;
            }

            patternObj.rolePattern.map((assignment) => {
                if((assignment.date === formatedDate) && (assignment.roleId === roleIdTechnician)) {
                    let startTime = moment((formatedDate + ' ' + assignment.startTime),"DD/MM/YYYY HH.mm");
                    let endTime = moment((formatedDate + ' ' + assignment.endTime),"DD/MM/YYYY HH.mm");

                    if((__now.diff(startTime) > 0) && (endTime.diff(__now))) {
                        result = __now;
                        return;
                    } else if ((startTime.diff(__now) > 0)) {
                        __nextAssignment = (startTime.diff(__nextAssignment) > 0) ? startTime: __nextAssignment;
                        result =  __nextAssignment;
                    }
                }
            });
        }

        return result;
    }

    /**
     * @param userId user's ID
     * @param specificdate date to calculate remaininghours based on allocation for this date
     * @param time Optional. specific time to get remaining effort. Should be in 24h time in the format hh.mm
     * @returns remaining tech time in hours
     */
    private static getRemainingTechTime(userId: string, specificdate: string, time?: string): number {
        let timeNow = time ? time: (new moment().format("HH.mm")); 
        let  patternCont: boolean  = true;
        const formatedDate = moment(new Date(specificdate)).format("YYYY/MM/DD");
        const roleIdTechnician = "R001";
        let result = 0;
        for(const patternObj of CalendarFilter.getCalendarPattern()) {
            if(patternObj.userId != userId) {
                continue;
            }
            patternObj.rolePattern.map((assignment) => {
                if(patternCont && this.isFutureDate(formatedDate,assignment.date) && (result <= 40)) {
                    if(assignment.roleId === roleIdTechnician) {
                        let startTime = moment((formatedDate + ' ' + assignment.startTime),"DD/MM/YYYY HH.mm");
                        let endTime = moment((formatedDate + ' ' + assignment.endTime),"DD/MM/YYYY HH.mm");
                        // time now wraped in moment to perform moment operations
                         let now = moment((formatedDate + ' ' + timeNow),"DD/MM/YYYY HH.mm");
                        let __timeRemaining = 0;
                        if(now.diff(startTime) > 0) {
                             // remaining time from timeNow to end time of assignment. if the job finished before 
                             // timeNow this will be a minus value.
                            __timeRemaining = endTime.diff(now);
                            if(__timeRemaining > 0) {
                                result += __timeRemaining/(60 * 60 * 1000);
                            }
                        } else if (startTime.diff(now) > 0){
                              __timeRemaining = endTime.diff(startTime);
                             result += __timeRemaining/(60 * 60 * 1000);
                        }
                    } else {
                        patternCont = false;
                        return;
                    }
                }
            })
        }

         return ((result > 40) ? 40: result);
    }

    /**
     * checks if given date is a future date agains start date parameter
     * @param startDate current date in YYY/MM/DD format
     * @param endDate end date in YYY/MM/DD format
     * @param true if future date, false otherwise
     */
    private static isFutureDate(startDate: string, endDate: string): boolean {
        // let futureDate: boolean = false;
        let _mStartDate = moment(startDate, "YYYY/MM/DD");
        let _mEndDate = moment(endDate, "YYYY/MM/DD");

        return (_mEndDate.diff(_mStartDate) > 0);
        // return futureDate;
    }

    /**
     * Adds new techfreeTime object to the data
     * @param userId user's ID
     * @param date Should be in ISO format. eg: 2017-03-06T09:48:00.000Z
     */
    private addTechFreePoints(userId: string, date: string){

        let userData = []
        LoginUser.find({ 'userId': userId })((user)=>{
           userData = user
        })
        
        let skillData = userData['skill'].map((skill)=>{
            return skill.skillId
        })

        let newTechFreePoint = {
            techFreeTime: date,
            userId: userId,
            skill: skillData,
            isAllocated:'0',
            timeRemaning: SequencingController.getRemainingTechTime(userId, date) + '',
            isStarted:'0'
        }

        return newTechFreePoint

    }

    /**
     * returns the current and next assigned jobs for a user
     * @param userId user's ID
     * @param date Should be in ISO format. eg: 2017-03-06T09:48:00.000Z
     */
    public getUserCurrentNextJobs(userID: string){
        let allocatedData = []
        let jobData = []
        UserAllocation.find({}).then((allocations)=>{
            allocatedData = allocations
        }) 
        Job.find({}).then((jobs)=>{
            jobData = jobs
        }) 

        let filteredAllocations = allocatedData.filter((allocation)=>{
            return allocation.userId == userID && (parseInt(allocation.jobStatus) == 0 || parseInt(allocation.jobStatus) == 1 )
        })

        if(filteredAllocations[0] != null){
            let allocations = filteredAllocations.map((allocation)=>{
                let Jobs = jobData.filter((job)=>{
                    return job.jobID == allocation.jobID
                })

                Jobs = JSON.parse(JSON.stringify(Jobs))

                // if(typeof Job[0].jobLineList == 'string'){                        
                //     Job[0].jobLineList = JSON.parse(Job[0].jobLineList)
                // }if(typeof Job[0].completedCheklistItems == 'string'){
                //     Job[0].completedCheklistItems = JSON.parse(Job[0].completedCheklistItems)
                // }
                return {
                    jobData : Jobs[0],
                    jobStatus: allocation.jobStatus 
                }
            })
            return allocations

        }else{       //no jobs assigned for the user
            return [] 
        }
    }

    public setCompleteAssignedJob(userId: string, date: string){
        let allocatedData = []
        UserAllocation.find({}).then((allocations)=>{
            allocatedData = allocations
        }) 

        allocatedData.forEach((allocation)=>{
            if(allocation.userId == userId){
                if(parseInt(allocation.jobStatus) == 0){
                    allocation.completedTime = date
                    allocation.jobStatus = '1000'
                }else if(parseInt(allocation.jobStatus) < 1000){
                    allocation.jobStatus = ''+ (parseInt(allocation.jobStatus) - 1)
                }
            }
        })

    }

    public startAssignedJob(userId: string, date: string){
        let allocatedData = []
        
        UserAllocation.find({}).then((allocations)=>{
            allocatedData = allocations
        }) 

        for(let allocation in allocatedData){
            if(allocatedData[allocation].userId == userId && parseInt(allocatedData[allocation].jobStatus) == 0){
                allocatedData[allocation].startedTime = date
                break;
            }
        }
    }

    public getColor(jobID: string, scheduledCompleteDate: string){
        let selectedJob = []
        Job.find({ 'jobID': jobID })((job)=>{
            selectedJob = job
        })

        let lastSection = selectedJob[0].sections[selectedJob[0].sections.length-1]

        let allocations = []
        UserAllocation.find({}).then((alc)=>{
            allocations = UserAllocation
        })

        let lastIsAllocated = allocations.filter((allocation)=>{
            return parseInt(allocation.jobStatus) == selectedJob[0].sections.length
        })
        if(lastIsAllocated[0] == null){
            return '#800080'    //purple
        }else{
            let plannedCompletionTime = SequencingController.getEndDate(lastIsAllocated[0].techFreeTime, parseFloat(lastSection[2]))

            let plannedDate = moment(new Date(plannedCompletionTime)).format("YYYY/MM/DD")
            let actualDate = moment(new Date(scheduledCompleteDate)).format("YYYY/MM/DD")

            if(plannedDate.diff(actualDate, 'days') < 0 ){
                return '#ff0000'        //Red
            }else if(plannedDate.diff(actualDate, 'days') == 0 ){
                return '#ffff00'        //Yellow
            }else{
                return '#00ff00'        //Green
            }

        }
    }
}