import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
import {Role} from '../../mongoose/roleModel';
import { DbControllerBase } from '../DbControllerBase';

export class RoleController extends DbControllerBase {
    private _server: Server;
 
    private static roleKeys = ['roleId', 'roleName', 'roleDescription'];

    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    constructor(server: Server, trackHistory: any, nextIdIncrement: number) {
        super(Role, RoleController.roleKeys, 'roleId', 'R00', nextIdIncrement);
        this._server = server;
        this.entitySingular = 'role';
        this.entityPlural = 'roles';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();
    }

    public getRouteList(): any[] {
        return [this.getAllEntitesRoute,this.getEntityByIdRoute,this.addEntity,this.updateEntity, this.deleteEntityById];
    }   
}

