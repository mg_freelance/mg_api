/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */
import {IEntity} from '../IEntity';

export class Role implements IEntity {

    private _roleId : number;
    private _roleName : string;
    private _roleDescription : string;

    get roleId() : number {return this._roleId;}

    get roleName() : String {return this._roleName;}

    get roleDescription() : String {return this._roleDescription;}

    getId() : any {return this._roleId;}
}