/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */
import { Server, Request, IReply } from 'hapi';
import { DbHandler } from './DbHandler';
const Boom = require('boom');

export abstract class DbControllerBase {
    private entityCollection: any;
    private entityHandler: DbHandler;

    protected entitySingular: string;
    protected entityPlural: string;
    protected idEntityName: string;
    protected idPrefix: string;
    protected nextIdIncrement: number;
    protected attributeList: any[];
    protected trackHistory: any;

    // DEFAULT ROUTES
    // GET
    public getAllEntitesRoute;
    // GET 
    public getEntityByIdRoute;
    // DELETE
    public deleteEntityById;
    //POST
    public addEntity;
    //PUT
    public updateEntity;

    public abstract getRouteList(): any[];

    /**
     * 
     */
    constructor(entityCollection: any, attributeList: any[],
        idEntityName: string, idPrefix: string, nextIdIncrement: number) {
        this.entityCollection = entityCollection;
        this.entityHandler = new DbHandler(this.entityCollection,idEntityName);
        this.idEntityName = idEntityName;
        this.nextIdIncrement = nextIdIncrement;

        this.attributeList = attributeList;
        this.entitySingular = 'entity';
        this.entityPlural = 'entites';
        this.idPrefix=idPrefix;
    }

    /**
     * 
     */
    public getRouteConfig(description: string): any {
        return ({
            auth: false,
            description: description,
            notes: description,
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }
        });
    }

    /**
     * 
     */
    protected entityAssigner = {
        set: (entityCollection) => {
            this.entityCollection = entityCollection;
        },
        get: () => {
            return this.entityCollection;
        }
    }


    initBaseRoutes() {
        this.getAllEntitesRoute = {
            method: "GET",
            path: "/" + this.entitySingular,
            config: this.getRouteConfig("Endpoint used to retrieve the list of " + this.entityPlural),
            handler: (request: Request, reply: IReply) => {
                let userListResponse = this.entityHandler.getList();
                reply(userListResponse);
            }

        };

        this.getEntityByIdRoute = {
            method: "GET",
            path: "/" + this.entitySingular + "/{" + this.idEntityName + "}",
            config: this.getRouteConfig("Endpoint used to retrieve a " + this.entitySingular + " by " + this.idEntityName),
            handler: (request: Request, reply: IReply) => {
                var filtered = this.entityHandler.get(this.idEntityName, request.params[this.idEntityName]);
                if (filtered.length == 0) {
                    reply(Boom.notFound(this.entitySingular + " not found"));
                } else {
                    reply(filtered);
                }
            }
        };
        this.deleteEntityById = {
            method: "DELETE",
            path: "/" + this.entitySingular + "/{" + this.idEntityName + "}",
            config: (this.getRouteConfig("Endpoint used to delete a " + this.entitySingular)),
            handler: (request: Request, reply: IReply) => {
                if (this.entityHandler.delete(this.idEntityName, request.params[this.idEntityName])) {
                    reply({ "status": 200, "message": "Successfully Deleted" });
                } else {
                    reply(Boom.notFound(this.entitySingular + " not found"));
                }
            }
            /**
            * REPLACE AFTER ADDING NECESSARY FIELDS TO THE DATAMODEL
            *  
            *
                handler: (request: Request, reply: IReply) => {
                    var filtered = this.entityHandler.get(this.idEntityName, request.params[this.idEntityName]);
                    if (filtered.length == 0) {
                        reply(Boom.notFound(this.entitySingular + " not found"));
                    } else {
                        filtered[0].deletedBy = request.headers['userAuthorized']
                        filtered[0].deletedTimeStamp = new Date().toISOString()
                        filtered[0].isDeleted = true

                        this.entityHandler.update(this.idEntityName, request.params[this.idEntityName], filtered[0]);
                        reply({ "status": 200, "message": "Successfully Deleted" });
                    }
                    
                }
                    

            * 
            */

            

        };
        this.addEntity = {
            method: "POST",
            path: "/" + this.entitySingular,
            config: this.getRouteConfig("Endpoint used to add a new " + this.entitySingular),
            handler: (request: Request, reply: IReply) => {
                var newElem = {};
                for (var elem in this.attributeList) {
                    newElem[this.attributeList[elem]] = request.payload[this.attributeList[elem]];
                }
                newElem[this.idEntityName] = this.idPrefix + (++this.nextIdIncrement);
            /**
            * UNCOMMENT AFTER ADDING NECESSARY FIELDS TO THE DATAMODEL
            *  
                newElem['createdBy'] = request.headers['userAuthorized']
                newElem['createdTimeStamp'] = new Date().toISOString()
                newElem['isDeleted'] = false

            */
                this.entityHandler.create(newElem);
                reply({ "status": 200, "message": "Successfully Created", "id": newElem[this.idEntityName]});
            }
        };


        this.updateEntity = {
            method: "PUT",
            path: "/" + this.entitySingular + "/{" + this.idEntityName + "}",
            config: this.getRouteConfig("Endpoint used to update and existing " + this.entitySingular),
            handler: (request: Request, reply: IReply) => {
            /**
            * UNCOMMENT AFTER ADDING NECESSARY FIELDS TO THE DATAMODEL
            *  
                request.payload['modifiedBy'] = request.headers['userAuthorized']
                request.payload['modifiedTimeStamp'] = new Date().toISOString()

            */
                this.entityHandler.update(this.idEntityName, request.params[this.idEntityName],request.payload);
                reply({ "status": 200, "message": "Successfully Updated" });
            }
        };
    }


}