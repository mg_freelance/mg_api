import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
import { Sprint } from '../../mongoose/sprintModel';
import { DbControllerBase } from '../DbControllerBase';
const Boom = require('boom');

export class SprintController extends DbControllerBase {
    private _server: Server;
    private static sprintKeys = [
        'sprintID',
        'userId',
        'jobID',
        'sprintType',
        'timeLogged',
        'checklist'
    ];

    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    constructor(server: Server, trackHistory: any) {
        super(Sprint, SprintController.sprintKeys, 'sprintID', '', 1);
        this._server = server;
        this.entitySingular = 'sprint';
        this.entityPlural = 'sprints';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();

    }

    private getSprintbyUserJob = {
        method: "GET",
        path: "/sprintByUserAndJob",
        config: {
            auth: false,
            description: "Endpoint used to get sprints by userId of JobId",
            notes: "Endpoint used to get sprints by userId of JobId",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {

            let query:any = {};
            const userId = request.query.userId;
            const jobId = request.query.jobID;

            if(userId && jobId){
                query.userId = userId;
                query.jobID = jobId;
            }else if(userId) {
                query.userId = userId;
            } else if(jobId){
                query.jobID = jobId;
            }

            return Sprint.find(query, (err, sprint)=>{
                if(err) reply(Boom.notFound('Couldnt find a job'));
                reply(sprint);
            })
        }
    }

    public getRouteList(): any[] {
        return [this.getAllEntitesRoute, this.getSprintbyUserJob, this.addEntity, this.updateEntity, this.deleteEntityById];
    }
}