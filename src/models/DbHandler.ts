/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */

/**
 * Generic class to handle all the general CRUD operations of a FlowLogic Entity.
 * 
 */

export class DbHandler {
    private model: any;
    private key: any;

    /**
     * 
     */
    constructor(model: any, key: any) {
        this.model = model;
        this.key = key;
    }

    /**
     * 
     */
    public create(entity: any): void {
        this.model.create(entity).then((response)=>{
            console.log(response)
        });
    }

    /**
     * 
     */
    public update(refId: any, refValue: any, payload: any): boolean {
        var filter = {};
        filter[refId] = refValue;

        return this.model
            .findOne(filter)
            .then((modelInstance) => {
                for (var attribute in payload) {
                    if (payload.hasOwnProperty(attribute) && attribute !== this.key && attribute !== "_id") {
                        modelInstance[attribute] = payload[attribute];
                    }
                }

                return modelInstance.save();
            })
            .then((modelInstance) => {
                return true;
            });
    }

    /**
     * 
     */
    public delete(refId: any, refValue: any): Boolean {
        const filter = {};
        filter[refId] = refValue;

        return this.model
            .remove(filter)
            .then(() => {
                return true;
            })
    }


    /**
     * 
     */
    public getList(): any {
        return this.model.find({})
            .then((modelInstances) => {
                return modelInstances;
            });
    }

    /**
     * 
     */
    public get(refId: any, refValue: any): any {
        var filter = {};
        filter[refId] = refValue;

        return this.model
            .findOne(filter)
            .then((modelInstance) => {
                return [modelInstance];
            });
    }
}