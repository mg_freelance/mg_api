import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
import { Workflow } from '../../mongoose/workflowModel';
import { DbControllerBase } from '../DbControllerBase';
const Boom = require('boom');

export class WorkflowController extends DbControllerBase {
    private _server: Server;

    private static departmentKeys = [
        'workflowId', 
        'workflowName',
        'SkillDept', 
        'TechOpen', 
        'Buffers',
        'checkpoints', 
        'checkpointOrder'
        ];

    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    constructor(server: Server, trackHistory: any, nextIdIncrement: number) {
        super(Workflow, WorkflowController.departmentKeys, 'workflowId', 'WF00', nextIdIncrement);
        this._server = server;
        this.entitySingular = 'workflow';
        this.entityPlural = 'workflows';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();
        
    }
    
    private getAllWorkflows = {
        method: "GET",
        path: "/workflows",
        config: {
            auth: false,
            description: "Endpoint used to get all Workflows",
            notes: "Endpoint used to get a all Workflows",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            let workflowSummery = []
            return Workflow.find({})
            .then((workflow)=>{
                workflow.forEach((flow)=>{
                    workflowSummery.push({
                        workflowId: flow.workflowId,
                        workflowName: flow.workflowName,
                    })
                })                
                reply(workflowSummery)
            })

            // var filteredList = [];
            // for (var elem in this.entityAssigner.get()) {
            //     filteredList.push({
            //         workflowId: this.entityAssigner.get()[elem].workflowId,
            //         workflowName: this.entityAssigner.get()[elem].workflowName,
            //     });
            // }
// =======
//             Workflow.find({}, (err, workflows)=>{
//                 if(err) reply(Boom.notFound('Couldnt Find Workflow'));
//                 reply(workflows);
//             })
// >>>>>>> e33ed82d75f0151eb7e01f78a2b62d5f10b7a745
        }
    }

    public getRouteList(): any[] {
        return [this.getAllWorkflows, this.getEntityByIdRoute, this.addEntity, this.updateEntity, this.deleteEntityById];
    }
}