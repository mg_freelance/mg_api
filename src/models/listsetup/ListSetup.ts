
export class ListSetup {

    private listSetupData: IListSetupModel[] = [];


    public getSetupLevels() {
        return { "LevelList": this.listSetupData };
    }

    public setupLevels(l: any) {
        let model: IListSetupModel = l;
        let matchFoundPos: number = -1;

        for (let x = 0; x < this.listSetupData.length; x++) {
            if (model.setupId === this.listSetupData[x].setupId) {
                matchFoundPos = x;
            }
        }

        if (matchFoundPos === -1) {
            this.listSetupData.push(model);
        }
    }

    public updateLevels(l: any) {
        let model: IListSetupModel = l;
        let matchFoundPos: number = -1;

        for (let x = 0; x < this.listSetupData.length; x++) {
            if (model.setupId === this.listSetupData[x].setupId) {
                matchFoundPos = x;
            }
        }

        if (matchFoundPos !== -1) {
            this.listSetupData[matchFoundPos] = model;
        }
    }


    public deleteLevels(l: any) {
        let model: IListSetupModel = l;
        let matchFoundPos: number = -1;

        for (let x = 0; x < this.listSetupData.length; x++) {
            if (model.setupId === this.listSetupData[x].setupId) {
                matchFoundPos = x;
            }
        }

        if (matchFoundPos !== -1 && model.name) {
            this.listSetupData[matchFoundPos] = model;
        } else if (matchFoundPos !== -1 && !model.name) {
            this.listSetupData.splice(matchFoundPos, 1);
        }
    }

}
export default new ListSetup();

export interface IListSetup {
    name: String;
    levelOne?: [{
        name?: String;
        levelTwo?: any[];
    }];

}

export interface IListSetupModel {
    setupId: String;
    name: String;
    levelOne?: [{
        name: String;
        levelTwo: any[];
    }];
}

export interface ILevelValue {
    parent: String;
    levelOne?: String;
    levelTwo?: String;
}