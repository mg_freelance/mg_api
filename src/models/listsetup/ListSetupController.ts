import { Server, Request, IReply } from 'hapi';
import { ControllerBase } from '../ControllerBase';
import ListSetup from "./ListSetup";
const Boom = require('boom');

export class ListSetupController {

    private getListSetup = {
        method: "GET",
        path: "/listsetup",
        config: {
            auth: false,
            description: "Endpoint used to get set up values.",
            notes: "Endpoint used to get set up values.",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            reply(ListSetup.getSetupLevels());
        }
    };

    private setListSetup = {
        method: "POST",
        path: "/listsetup",
        config: {
            auth: false,
            description: "Endpoint used to set up values in 3 levels.",
            notes: "Endpoint used to set up values in 3 levels.",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            if (!request.payload.setupId) {
                reply(Boom.badRequest('Required values are missing.'));
            } else {
                ListSetup.setupLevels(request.payload);
                reply({ "status": 200, data: "Added Successfully." });
            }
        }
    };

    private updateListSetup = {
        method: "PUT",
        path: "/listsetup",
        config: {
            auth: false,
            description: "Endpoint used to update values in 3 levels.",
            notes: "Endpoint used to update values in 3 levels.",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            if (!request.payload.setupId) {
                reply(Boom.badRequest('Required values are missing.'));
            } else {
                ListSetup.updateLevels(request.payload);
                reply({ "status": 200, data: "Added Successfully." });
            }
        }
    }

    private deleteListSetup = {
        method: "DELETE",
        path: "/listsetup",
        config: {
            auth: false,
            description: "Endpoint used to delete values in 3 levels.",
            notes: "Endpoint used to delete values in 3 levels.",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            if (!request.payload.setupId) {
                reply(Boom.badRequest('Required values are missing.'));
            } else {
                ListSetup.deleteLevels(request.payload);
                reply({ "status": 200, data: "Added Successfully." });
            }
        }
    }

    public getRouteList(): any[] {
        return [this.getListSetup, this.setListSetup, this.updateListSetup, this.deleteListSetup];
    }

    // -------------------- Lists Setup New --------------------

    private listStore: IListSetupModel[] = [];


    private addSampleData() {
        let sample1: IListSetupModel = <IListSetupModel>{};
        //sample1.setupId = 'LS' + new Date().getTime();
        sample1.setupId = 'LS1';
        sample1.name = 'Car';
        sample1.levelOne.push({
            name: 'Sedan',
            levelTwo: ['4WD', '2WD']
        });
        sample1.levelOne.push({
            name: 'Coupe',
            levelTwo: ['Sports', 'City']
        });
        this.addNewListModel(sample1);

        let sample2: IListSetupModel = <IListSetupModel>{};
        sample2.setupId = 'LS2';
        sample2.name = 'SUV';
        sample2.levelOne.push({
            name: '4 Wheel Drive',
            levelTwo: ['17 inch', '18 inch']
        });
        sample2.levelOne.push({
            name: '2 Wheel Drive',
            levelTwo: ['Country', 'Offroad']
        })
        this.addNewListModel(sample2);
    }

    private addNewListModel(model: IListSetupModel) {
        this.listStore.push(model);
    }

    private deleteModel(model: IListSetupModel) {
        if (model.name === null) {
            // remove from array
        } else {
            // update array with model
        }
    }

    private editModel(model: IListSetupModel) {
        // update array with model
    }

}

export interface IListSetupModel {
    setupId: String;
    name: String;
    levelOne?: [{
        name: String;
        levelTwo: any[];
    }];
}