/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */
import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
import { DbControllerBase } from '../DbControllerBase';
import  { Department } from  '../../mongoose/departmentModel';
/**
 * 
 */
export class DepartmentController extends DbControllerBase {
    private _server: Server;

    private static departmentKeys =
    ['departmentId', 'departmentName',
        'setup', 'checkpoints', 'checkpointsOrder'];

    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    /**
     * 
     */
    constructor(server: Server, trackHistory: any, nextIdIncrement: number) {
        super(Department , DepartmentController.departmentKeys, 'departmentId', 'Dept00', nextIdIncrement);
        this._server = server;
        this.entitySingular = 'department';
        this.entityPlural = 'departments';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();
    }
    
    /**
     * 
     */
    private getAllDepartments = {
        method: "GET",
        path: "/departments",
        config: {
            auth: false,
            description: "Endpoint used to get all Departments",
            notes: "Endpoint used to get a all Departments",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
             return Department.find({})
            .then((modelInstances) => {               
                reply(modelInstances)
            });
        }
    }

    /**
     * 
     */
    public getRouteList(): any[] {
        return [this.getAllDepartments, this.getEntityByIdRoute, this.addEntity, this.updateEntity, this.deleteEntityById];
    }
}