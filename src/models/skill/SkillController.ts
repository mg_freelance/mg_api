
import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
//import { Data } from '../../data/Data';
import {Skill} from '../../mongoose/skillModel';
import { DbControllerBase } from '../DbControllerBase';

export class SkillController extends DbControllerBase {
    private _server: Server;
    private static skillKeys = ['skillId', 'skillName', 'skillDescription'];
    private responses = {
        '200': {
            'description': 'Success'
        }
    };

    constructor(server: Server, trackHistory: any, nextIdIncrement: number ) {
        super(Skill, SkillController.skillKeys, 'skillId', 'SK00', nextIdIncrement);

        this._server = server;
        this.entitySingular = 'skill';
        this.entityPlural = 'skills';
        this.trackHistory = trackHistory;
        super.initBaseRoutes();
    }
    
     private getAllWorkflows = {
        method: "GET",
        path: "/allskills",
        config: {
            auth: false,
            description: "Endpoint used to get all Workflows",
            notes: "Endpoint used to get a all Workflows",
            tags: [
                'api', 'v1'
            ],
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success'
                        }
                    }
                }
            }

        },
        handler: (request: Request, reply: IReply) => {
            return Skill.find({})
            .then((modelInstances) => {
                var response = {};
                //response["skill"] = modelInstances;
                reply(modelInstances);
            });
        }
    }

    public getRouteList(): any[] {
        return [this.getAllWorkflows, this.getAllEntitesRoute,this.getEntityByIdRoute,this.addEntity,this.updateEntity,this.deleteEntityById];
    }   
}