/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */
import * as test from "./api-server/Server";
const server : test.APIServer = new test.APIServer();

server
    .init()
    .start((err) => {
        if (err) {
            throw err;
        }
        console.log("server running at 80");
    });
