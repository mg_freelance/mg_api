import { IEditLayoutOptions, ICoreLayoutOptions, JobSetupEditCriteriaSampleData } from "./JobSetupEditCriteriaSampleData";
import { Data } from "../Data";
import { SequencingController } from "../../models/process/SequncingController";
import { Job } from "../../mongoose/jobsModel";
import { Workflow } from "../../mongoose/workflowModel";
const moment = require('moment');

export interface IJobDataField {
    name: string,
    type: any,
    enable?: true,
    isCustomField?: boolean
}

export interface IJobsData {
    // ---------------------------- Planning View Data Attributes ----------------------------
    jobid: string;
    estimateid: string;
    vehicletype: string;
    customerName: string;
    onsite: boolean;
    parts: boolean;

    // ---------------------------- Schedule View EXTRA Data Attributes ----------------------------
    startDate: string;
    endDate: string;

    // ---------------------------- Sequence View EXTRA Data Attributes ----------------------------
    workflow: string;
    currentDept: string;

    // ---------------------------- Production View EXTRA Data Attributes ----------------------------
    vehicleReg: string;
    nextDept: string;

    // ---------------------------- Murphy View EXTRA Data Attributes ----------------------------
    murphyStatus: string;

    // ---------------------------- Completed View EXTRA Data Attributes ----------------------------
    vehicleYear: number;
    workflowCompletionDate: string;
}

export interface IJobsDataAll {
    planning: IJobsData[];
    schedule: IJobsData[];
    sequence: IJobsData[];
    production: IJobsData[];
    murphy: IJobsData[];
    complete: IJobsData[];
}

export interface IJobDataType {
    fieldName: string;
    value: string;
    inputType?: {
        name: string;
        id: string
    };
    IsSequenceTrigger?: boolean,
    lockedInProduction?: boolean
}


export class JobsTestData {
    private readonly VIEWS: string[] = ["PLANNING", "SCHEDULE", "SEQUENCE", "PRODUCTION", "MURPHY", "COMPLETE"];
    private JOB_DATA: IJobDataType[];

    constructor() {
        this.JOB_DATA = [];
    }

    public getDataForView(view: string, userId: string) {
        switch (view.toLocaleUpperCase()) {
            case this.VIEWS[0]:
                return this.getData_planning(userId);
            case this.VIEWS[1]:
                return this.getData_scheduling(userId);
            case this.VIEWS[2]:
                return this.getData_sequence(userId);
            case this.VIEWS[3]:
                return this.getData_production(userId);
            case this.VIEWS[4]:
                return this.getData_murphy(userId);
            case this.VIEWS[5]:
                return this.getData_complete(userId);
        }
    }

    public getAll(userId: string): IJobsDataAll {
        let data: IJobsDataAll = <IJobsDataAll>{};
        data.planning = this.getData_planning(userId);
        data.schedule = this.getData_scheduling(userId);
        data.sequence = this.getData_sequence(userId);
        data.production = this.getData_production(userId);
        data.murphy = this.getData_murphy(userId);
        data.complete = this.getData_complete(userId);

        return data;
    }

    private getJobData() {
        return Job.find({}).then((data) => {
            return data;
        })
    }

    private toCamelCase(str) {
        return str.replace(/^([A-Z])|\s(\w)/g, function (match, p1, p2, offset) {
            if (p2) return p2.toUpperCase();
            return p1.toLowerCase();
        });
    }

    private getData_planning(userId: string) {
        let _planningData: IJobDataType[] = [];
        let _dataList;
        let result = [];
        let resultList = [];

        return this.getJobData()
            .then((jobs) => {
               return this.getWorkFlows()
                    .then((workflows) => {
                        _dataList = jobs;
                        let _data: any;
                        
                        
                        // running through each job to verify if the job 
                        for (const elem in _dataList) {
                            let planDataResponseObj = <any>{};
                            // extracting attributes related to job information
                            _dataList[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    planDataResponseObj['jobID'] = _dataList.jobID;
                                } else{
                                    planDataResponseObj[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })
                            // extracting attributes related to job events
                            _dataList[elem]['jobEvents'].forEach((event)=> {
                                let suffix = '';
                                if(event.EventName == 'Starting Job'){
                                    suffix = 'StartDate';
                                } else{
                                    suffix = 'EndDate';
                                }
                                // setting up start/end dates for each type schedule,insequence,inproduction, incompletion
                                planDataResponseObj[this.toCamelCase('Scheduled') + suffix] = event['Scheduled'];
                                planDataResponseObj[this.toCamelCase('inSequence') + suffix] = event['inSequence'];
                                planDataResponseObj[this.toCamelCase('inProduction') + suffix] = event['inProduction'];
                                planDataResponseObj[this.toCamelCase('inCompletion') + suffix] = event['inCompletion'];
                            })
                            // pre defined attributes that are required in response, but not captured in above loops
                            planDataResponseObj['jobID'] = _dataList[elem].jobID;
                            planDataResponseObj['color'] = null;
                            planDataResponseObj['currentDept'] = _dataList[elem].jobProgress.currentDepartment[0];
                            planDataResponseObj['currentUser'] = _dataList[elem].jobProgress.currentDepartment[3];
                            planDataResponseObj['murphyStatus'] = _dataList[elem].murphy.currentState[0];
                            planDataResponseObj['workflow'] =  (this.getWorkFlowDataById(_dataList[elem].workflowID, workflows).value);
                            planDataResponseObj['workflowID'] =  _dataList[elem].workflowID;
                            planDataResponseObj['workflowFinishDate'] =  _dataList[elem].jobProgress.workflowFinishDate;
                            // running through the filtering logic for planning
                            if((planDataResponseObj[this.toCamelCase('Scheduled') + 'StartDate']).length > 0 ) {
                                // planning conciders only events without schedule date. confirmed by Rasika to use starting jobs schedule date
                                console.log('Job ' + planDataResponseObj.jobID + ' is having a schedule date as: ' 
                                + planDataResponseObj.scheduledStartDate + ', thus not picking up for plan views');
                            } else {
                                resultList.push(planDataResponseObj);
                            }
                        }

                        // TODO Check with @Chandima about getting edit criterias model
                        //let personalizedFilters: ICoreLayoutOptions[] = JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, "PLANNING");
                        // let filterData = result;

                        /*for(const filter of personalizedFilters) {
                            filterData = this.filterByCriteria(filter, filterData);
                        }*/
                        // return filterData;
                        return resultList;
                });
        });
        
    }

    private getData_scheduling(userId: string) {
        let _scheduleDataList: IJobDataType[] = [];
        // let _dataList = Data.jobs;
        let _dataList;
        let result = [];
        let resultList = [];
        return this.getJobData()
            .then((jobs) => {
               return this.getWorkFlows()
                    .then((workflows) => {
                        _dataList = jobs;
                        let _data: any;
                        
                        
                        // running through each job to verify if the job 
                        for (const elem in _dataList) {
                            let scheduleDataResponseObj = <any>{};
                            let __inSequencValue: boolean = true;
                            // extracting attributes related to job information
                            _dataList[elem]['jobInfo'].forEach((item)=> {
                                if (item[0].InSequenceTrigger.toString().toLowerCase() !== "false") {
                                    __inSequencValue = (__inSequencValue && item[1].toString().toLowerCase()!=="false"  && item[1].toString().toLowerCase()!=="");
                                } 
                                if(item[0].fieldName == 'Job ID'){
                                    scheduleDataResponseObj['jobID'] = _dataList[elem].jobID;
                                } else{
                                    scheduleDataResponseObj[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })
                            // extracting attributes related to job events
                            _dataList[elem]['jobEvents'].forEach((event)=> {
                                let suffix = '';
                                if(event.EventName == 'Starting Job'){
                                    suffix = 'StartDate';
                                } else{
                                    suffix = 'EndDate';
                                }
                                // setting up start/end dates for each type schedule,insequence,inproduction, incompletion
                                scheduleDataResponseObj[this.toCamelCase('Scheduled') + suffix] = event['Scheduled'];
                                scheduleDataResponseObj[this.toCamelCase('inSequence') + suffix] = event['inSequence'];
                                scheduleDataResponseObj[this.toCamelCase('inProduction') + suffix] = event['inProduction'];
                                scheduleDataResponseObj[this.toCamelCase('inCompletion') + suffix] = event['inCompletion'];
                            })
                            // pre defined attributes that are required in response, but not captured in above loops
                            scheduleDataResponseObj['jobID'] = _dataList[elem].jobID;
                            scheduleDataResponseObj['color'] = null;
                            scheduleDataResponseObj['currentDept'] = _dataList[elem].jobProgress.currentDepartment[0];
                            scheduleDataResponseObj['currentUser'] = _dataList[elem].jobProgress.currentDepartment[3];
                            scheduleDataResponseObj['murphyStatus'] = _dataList[elem].murphy.currentState[0];
                            scheduleDataResponseObj['workflow'] =  (this.getWorkFlowDataById(_dataList[elem].workflowID, workflows).value);
                            scheduleDataResponseObj['workflowID'] =  _dataList[elem].workflowID;
                            scheduleDataResponseObj['workflowFinishDate'] =  _dataList[elem].jobProgress.workflowFinishDate;
                            // running through the filtering logic for scheduling. shoudl have a schedule date And at least one of onsite OR parts-ready should be false
                            if(!__inSequencValue && ((scheduleDataResponseObj[this.toCamelCase('Scheduled') + 'StartDate']).length > 0) && 
                                ((scheduleDataResponseObj.partsReady.toString().toLowerCase() === "true") || (scheduleDataResponseObj.vehicleOnSite.toString().toLowerCase() === "true")) ) {
                                resultList.push(scheduleDataResponseObj);
                            }
                        }

                        // let personalizedFilters: ICoreLayoutOptions[] = JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, "SCHEDULE");
                        // let filterData = result;
                        /*for(const filter of personalizedFilters) {
                            filterData = this.filterByCriteria(filter, filterData);
                        }*/
                        return resultList;
                })
        });

    }

    private getData_sequence(userId: string) {
        let _seqDataList: IJobDataType[] = [];
        // let _dataList = Data.jobs;
        let _dataList;
        let result = [];
        let resultList = [];
        return this.getJobData()
            .then((jobs) => {
               return this.getWorkFlows()
                    .then((workflows) => {
                        let _data: any;
                        _dataList = jobs;
                        
                        
                        // running through each job to verify if the job matches the extraction criteria
                        for (const elem in _dataList) {
                            let seqDataResponseObj = <any>{};
                            let __inSequencValue: boolean = true;

                            // extracting attributes related to job information
                            _dataList[elem]['jobInfo'].forEach((item)=>{
                                // getting in sequence value
                                 if (item[0].InSequenceTrigger.toString().toLowerCase() !== "false") {
                                    __inSequencValue = (__inSequencValue && item[1].toString().toLowerCase()!=="false"  && item[1].toString().toLowerCase()!=="");
                                } 

                                if(item[0].fieldName == 'Job ID'){
                                    seqDataResponseObj['jobID'] = _dataList[elem].jobID;
                                } else{
                                    seqDataResponseObj[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })
                            // extracting attributes related to job events
                            _dataList[elem]['jobEvents'].forEach((event)=> {
                                let suffix = '';
                                if(event.EventName == 'Starting Job'){
                                    suffix = 'StartDate';
                                } else{
                                    suffix = 'EndDate';
                                }
                                // setting up start/end dates for each type schedule,insequence,inproduction, incompletion
                                seqDataResponseObj[this.toCamelCase('Scheduled') + suffix] = event['Scheduled'];
                                seqDataResponseObj[this.toCamelCase('inSequence') + suffix] = event['inSequence'];
                                seqDataResponseObj[this.toCamelCase('inProduction') + suffix] = event['inProduction'];
                                seqDataResponseObj[this.toCamelCase('inCompletion') + suffix] = event['inCompletion'];
                            })
                            // pre defined attributes that are required in response, but not captured in above loops
                            seqDataResponseObj['jobID'] = _dataList[elem].jobID;
                            seqDataResponseObj['color'] = this.getRowColor(_dataList[elem], seqDataResponseObj.inSequenceEndDate);
                            seqDataResponseObj['currentDept'] = _dataList[elem].jobProgress.currentDepartment[0];
                            seqDataResponseObj['currentUser'] = _dataList[elem].jobProgress.currentDepartment[3];
                            seqDataResponseObj['murphyStatus'] = _dataList[elem].murphy.currentState[0];
                            seqDataResponseObj['workflow'] =  (this.getWorkFlowDataById(_dataList[elem].workflowID, workflows).value);
                            seqDataResponseObj['workflowID'] =  _dataList[elem].workflowID;
                            seqDataResponseObj['workflowFinishDate'] =  _dataList[elem].jobProgress.workflowFinishDate;
                            // running through the filtering logic for scheduling. Shoudl have a schedule date And both onsite OR parts-ready should be true
                            if(__inSequencValue && ((seqDataResponseObj[this.toCamelCase('Scheduled') + 'StartDate']).length > 0) ) {
                                resultList.push(seqDataResponseObj);
                            }
                        }

                        // let personalizedFilters: ICoreLayoutOptions[] = JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, "SEQUENCE");
                        // let filterData = result;
                        /*for(const filter of personalizedFilters) {
                            filterData = this.filterByCriteria(filter, filterData);
                        }*/
                        return resultList;
                    });
            });         
    }

    private getData_production(userId: string) {
        let _prodDataList: IJobDataType[] = [];
        // let _dataList = Data.jobs;
        let _dataList;
        let result = [];
        let resultList = [];
        return this.getJobData()
            .then((jobs) => {
               return this.getWorkFlows()
                    .then((workflows) => {
                        let _data: any;
                        _dataList = jobs;
                        
                        
                        // running through each job to verify if the job 
                        for (const elem in _dataList) {
                        let prodDataResponseObj = <any>{};
                        let __inSequencValue: boolean = true;
                            // extracting attributes related to job information
                            _dataList[elem]['jobInfo'].forEach((item)=>{
                            
                            if (item[0].InSequenceTrigger.toString().toLowerCase() !== "false") {
                                    __inSequencValue = (__inSequencValue && item[1].toString().toLowerCase()!=="false"  && item[1].toString().toLowerCase()!=="");
                                } 
                                if(item[0].fieldName == 'Job ID'){
                                    //prodDataResponseObj['jobID'] = _dataList.jobID;
                                } else{
                                    prodDataResponseObj[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })
                            // extracting attributes related to job events
                            _dataList[elem]['jobEvents'].forEach((event)=> {
                                let suffix = '';
                                if(event.EventName == 'Starting Job'){
                                    suffix = 'StartDate';
                                } else{
                                    suffix = 'EndDate';
                                }
                                // setting up start/end dates for each type schedule,insequence,inproduction, incompletion
                                prodDataResponseObj[this.toCamelCase('Scheduled') + suffix] = event['Scheduled'];
                                prodDataResponseObj[this.toCamelCase('inSequence') + suffix] = event['inSequence'];
                                prodDataResponseObj[this.toCamelCase('inProduction') + suffix] = event['inProduction'];
                                prodDataResponseObj[this.toCamelCase('inCompletion') + suffix] = event['inCompletion'];
                            })
                            // pre defined attributes that are required in response, but not captured in above loops
                            prodDataResponseObj['jobID'] = _dataList[elem].jobID;
                            prodDataResponseObj['color'] = this.getRowColor(_dataList[elem], prodDataResponseObj.inProductionEndDate);
                            prodDataResponseObj['currentDept'] = _dataList[elem].jobProgress.currentDepartment[0];
                            prodDataResponseObj['currentUser'] = _dataList[elem].jobProgress.currentDepartment[3];
                            prodDataResponseObj['murphyStatus'] = _dataList[elem].murphy.currentState[0];
                            prodDataResponseObj['workflow'] =  (this.getWorkFlowDataById(_dataList[elem].workflowID, workflows).value);
                            prodDataResponseObj['workflowID'] =  _dataList[elem].workflowID;
                            prodDataResponseObj['workflowFinishDate'] =  _dataList[elem].jobProgress.workflowFinishDate;
                            
                            // running through the filtering logic for scheduling. shoudl have a schedule date And at least one of onsite OR parts-ready should be false
                            if(((prodDataResponseObj[this.toCamelCase('Scheduled') + 'StartDate']).length > 0) &&
                                 (prodDataResponseObj.currentDept.length > 0) && __inSequencValue) {
                                resultList.push(prodDataResponseObj);
                            }
                        }

                        // let personalizedFilters: ICoreLayoutOptions[] = JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, "PRODUCTION");
                        // let filterData = result;
                        /*for(const filter of personalizedFilters) {
                            filterData = this.filterByCriteria(filter, filterData);
                        }*/
                        return resultList;
                    });
            });
    }

    private getData_murphy(userId: string) {
        let _murphyDataList: IJobDataType[] = [];
        // let _dataList = Data.jobs;
        let _dataList;
        let result = [];
        let resultList = [];
        return this.getJobData()
            .then((jobs) => {
               return this.getWorkFlows()
                    .then((workflows) => {
                         let _data: any;
                        _dataList = jobs;
                        
                        
                        // running through each job to verify if the job 
                        for (const elem in _dataList) {
                        let murphyDataResponseObj = <any>{};
                            // extracting attributes related to job information
                            _dataList[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                   // murphyDataResponseObj['jobID'] = _dataList.jobID;
                                } else{
                                    murphyDataResponseObj[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })
                            // extracting attributes related to job events
                            _dataList[elem]['jobEvents'].forEach((event)=> {
                                let suffix = '';
                                if(event.EventName == 'Starting Job'){
                                    suffix = 'StartDate';
                                } else{
                                    suffix = 'EndDate';
                                }
                                // setting up start/end dates for each type schedule,insequence,inproduction, incompletion
                                murphyDataResponseObj[this.toCamelCase('Scheduled') + suffix] = event['Scheduled'];
                                murphyDataResponseObj[this.toCamelCase('inSequence') + suffix] = event['inSequence'];
                                murphyDataResponseObj[this.toCamelCase('inProduction') + suffix] = event['inProduction'];
                                murphyDataResponseObj[this.toCamelCase('inCompletion') + suffix] = event['inCompletion'];
                            })
                            // pre defined attributes that are required in response, but not captured in above loops
                            murphyDataResponseObj['jobID'] = _dataList[elem].jobID;
                            murphyDataResponseObj['color'] = this.getRowColor(_dataList[elem], murphyDataResponseObj.inProductionEndDate);
                            murphyDataResponseObj['currentDept'] = _dataList[elem].jobProgress.currentDepartment[0];
                            murphyDataResponseObj['currentUser'] = _dataList[elem].jobProgress.currentDepartment[3];
                            murphyDataResponseObj['murphyStatus'] = _dataList[elem].murphy.currentState[0];
                            murphyDataResponseObj['workflow'] =  (this.getWorkFlowDataById(_dataList[elem].workflowID, workflows).value);
                            murphyDataResponseObj['workflowID'] =  _dataList[elem].workflowID;
                            murphyDataResponseObj['workflowFinishDate'] =  _dataList[elem].jobProgress.workflowFinishDate;
                            
                            // running through the filtering logic for scheduling. shoudl have a schedule date And at least one of onsite OR parts-ready should be false
                            if((murphyDataResponseObj.murphyStatus.length > 0) && (murphyDataResponseObj.workflowFinishDate.length <= 0)) {
                                resultList.push(murphyDataResponseObj);
                            }
                        }

                        // let personalizedFilters: ICoreLayoutOptions[] = JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, "MURPHY");
                        // let filterData = result;
                        /*for(const filter of personalizedFilters) {
                            filterData = this.filterByCriteria(filter, filterData);
                        }*/
                        return resultList;
                    });
            });
    }

    private getData_complete(userId: string) {
        let _completeDataList: IJobDataType[] = [];
        // let _dataList = Data.jobs;
        let _dataList;
        let result = [];
        let resultList = [];
        return this.getJobData()
            .then((jobs) => {
               return this.getWorkFlows()
                    .then((workflows) => {
                        let _data: any;
                        _dataList = jobs;
                        let compDataResponseObj = <any>{};
                        
                        // running through each job to verify if the job 
                        for (const elem in _dataList) {
                        
                            // extracting attributes related to job information
                            _dataList[elem]['jobInfo'].forEach((item)=>{
                                if(item[0].fieldName == 'Job ID'){
                                    //compDataResponseObj['jobID'] = _dataList.jobID;
                                } else{
                                    compDataResponseObj[this.toCamelCase(item[0].fieldName)] = item[1]
                                }
                            })
                            // extracting attributes related to job events
                            _dataList[elem]['jobEvents'].forEach((event)=> {
                                let suffix = '';
                                if(event.EventName == 'Starting Job'){
                                    suffix = 'StartDate';
                                } else{
                                    suffix = 'EndDate';
                                }
                                // setting up start/end dates for each type schedule,insequence,inproduction, incompletion
                                compDataResponseObj[this.toCamelCase('Scheduled') + suffix] = event['Scheduled'];
                                compDataResponseObj[this.toCamelCase('inSequence') + suffix] = event['inSequence'];
                                compDataResponseObj[this.toCamelCase('inProduction') + suffix] = event['inProduction'];
                                compDataResponseObj[this.toCamelCase('inCompletion') + suffix] = event['inCompletion'];
                            })
                            // pre defined attributes that are required in response, but not captured in above loops
                            compDataResponseObj['jobID'] = _dataList[elem].jobID;
                            compDataResponseObj['color'] = this.getRowColor(_dataList[elem], compDataResponseObj.inCompletionEndDate);
                            compDataResponseObj['currentDept'] = _dataList[elem].jobProgress.currentDepartment[0];
                            compDataResponseObj['currentUser'] = _dataList[elem].jobProgress.currentDepartment[3];
                            compDataResponseObj['murphyStatus'] = _dataList[elem].murphy.currentState[0];
                            compDataResponseObj['workflow'] =  (this.getWorkFlowDataById(_dataList[elem].workflowID, workflows).value);
                            compDataResponseObj['workflowID'] =  _dataList[elem].workflowID;
                            compDataResponseObj['workflowFinishDate'] =  _dataList[elem].jobProgress.workflowFinishDate;
                            
                            // running through the filtering logic for scheduling. shoudl have a schedule date And at least one of onsite OR parts-ready should be false
                            if(compDataResponseObj.workflowFinishDate.length > 0) {
                                resultList.push(compDataResponseObj);
                            }
                        }

                        // let personalizedFilters: ICoreLayoutOptions[] = JobSetupEditCriteriaSampleData.getInstance().getEditCriteria(userId, "COMPLETE");
                        // let filterData = result;
                        /*for(const filter of personalizedFilters) {
                            filterData = this.filterByCriteria(filter, filterData);
                        }*/
                        return resultList;
                    });
            });
    }
    /**
     * Checks for row color 
     */
    //private getRowColor(jobID: string, scheduledCompleteDate: string): string {
    private getRowColor(selectedJob: any, scheduledCompleteDate: string): string {
        let lastSection = selectedJob.sections[selectedJob.sections.length-1]

        let lastIsAllocated = Data.userAllocation.filter((allocation)=>{
            return parseInt(allocation.jobStatus) == selectedJob.sections.length
        })
        if(lastIsAllocated[0] == null){
            return '#800080'    //purple
        } else{
            const __hrsToAdd = lastSection ? parseFloat(lastSection[2]): parseFloat('0');
            let plannedCompletionTime = SequencingController.getEndDate(lastIsAllocated[0].techFreeTime, __hrsToAdd);

            let plannedDate = moment(new Date(plannedCompletionTime)).format("YYYY/MM/DD")
            let actualDate = moment(new Date(scheduledCompleteDate)).format("YYYY/MM/DD")
            
            try {
                if(plannedDate.diff(actualDate, 'days') < 0 ){
                    return '#ff0000'        //Red
                }else if(plannedDate.diff(actualDate, 'days') == 0 ){
                    return '#ffff00'        //Yellow
                }else{
                    return '#00ff00'        //Green
                }
            } catch (error) {
                return null;
            }
            

        }
    }

    private getCurrentDept(_data: any): IJobDataType {
        let _job: IJobDataType = <IJobDataType>{};
        _job.fieldName = 'currentDept';
        // todo check for array element values and if we need to add other Attributes
        _job.value = _data.jobProgress.currentDepartment[0];

        return _job;
    }


    private getWorkFlowDataById(id: string, _workflows?): IJobDataType {
        // todo where to find workflow info
        let _job: IJobDataType = <IJobDataType>{};
        let workflows = _workflows;
            _job.fieldName = 'workflow';
            _job.value = '';

            let _wf;
            for (_wf of workflows) {
                if (_wf.workflowId === id) {
                    // return _wf;
                    _job.fieldName = 'workflow';
                    _job.value = _wf.workflowName;
                    break;
                }
            }
        return _job;
    }
    
    private getMurphyData(_data: any): IJobDataType {
        let _job: IJobDataType = <IJobDataType>{};

        _job.fieldName = 'murphyStatus';
        _job.value = '';
        let _murphy;
        /*for (_murphy of _data.murphy) {
            if (_data.murphy[_murphy].length > 0) {
                _job.value = _data.murphy[_murphy][0];
                break;
            }
        }*/
        // TODO Check with @Chandima about getting the zeroth index
        if(_data.murphy.currentState.length > 0) {
            _job.value = _data.murphy.currentState[0];
        }
        return _job;
    }
    /**
     * Construct the data in key-value pair list object
     * @param data data to construct the response 
     */
    private constructResponse(data: IJobDataType[]): any {
        let _response = {};
        let _data: IJobDataType;
        for (_data of data) {
            _response[_data.fieldName] = _data.value;
        }
        return _response;
    }

    private filterByCriteria(criteria: ICoreLayoutOptions, data: any[]): any[] {
        let result: any[] = [];
        // if criteria filter is not selected
        if(!criteria.filter) {
            return data;
        }
        for (const item of data) {
            let _key = this.getKey(item, criteria.field);
            if(_key !== null) {
                if(this.doFilter(criteria.filtervalue, item[_key])) {
                    result.push(item);
                }
            }
        }

        return result;
    }

    private doFilter(operand, dataValue): boolean {
        let isPassing = false;

        if(operand === null) { return false};

        let operator = operand.split(" ")[0];
        let value = operand.split(" ")[1];

        if (operator == "<") {
            return (dataValue < value);
        } else if (operator == ">") {
            return (dataValue > value);
        } else if (operator == "=") {
            return (dataValue == value);
        }

        return isPassing;
    }

    private getKey(obj, field) {
        let keys: string[] = Object.keys(obj);
        for (const key of keys) {
            if(key.toUpperCase() === field.replace(/ /g,'').toUpperCase()) {
                return key;
            }
        }
        return null;
    }

    private getJobs() {
     return Job.find({}).then((data) => {
            return data;
        })
    }


    private getWorkFlows = () => {
        return Workflow.find({}).then(data => {
            return data;
        })
    }
}