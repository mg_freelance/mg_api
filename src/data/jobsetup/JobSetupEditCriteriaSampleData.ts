import { Data } from "../Data";

export class WidthOptions {
    public static SINGLE = "SINGLE";
    public static DOUBLE = "DOUBLE";
}

export class SortPriority {
    public static PRIMARY = "PRIMARY";
    public static SECONDARY = "SECONDARY";
}

export class SortOrder {
    public static ASCENDING = "ASCENDING";
    public static DESCENDING = "DESCENDING";
}

export type FieldOptionsTypes = "JOB ID" | "ESTIMATE ID" | "VEHICLE TYPE" | "CUSTOMER NAME" | "VEHICLE ON SITE" | "PARTS ON SITE";

export class FieldOptions {
    public static JOBID = "JOB ID";
    public static ESTIMATEID = "ESTIMATE ID";
    public static VEHICLETYPE = "VEHICLE TYPE";
    public static CUSTOMERNAME = "CUSTOMER NAME";
    public static VEHICLEONSITE = "VEHICLE ON SITE";
    public static PARTSONSITE = "PARTS ON SITE";
}


/**
 * Model that used to store layouts values be used in edit view. 
 */
export interface ICoreLayoutOptions {
    width: string;
    field: string;
    filter: boolean;
    filtervalue?: string;
}
/**
 * Extended layout values
 */
export interface IEditLayoutOptions extends ICoreLayoutOptions {
    sortPriority: SortPriority,
    sortOrder: SortOrder
}

/**
 * Model to store all filter criterias of each user.
 * These criterias are displayed in respective edit views. 
 */
export interface IJobSetupFilterCriterias {
    userId: string;
    planning: IEditLayoutOptions[];
    schedule: IEditLayoutOptions[];
    sequence: ICoreLayoutOptions[];
    production: IEditLayoutOptions[];
    murphy: IEditLayoutOptions[];
    complete: IEditLayoutOptions[];
}


export class JobSetupEditCriteriaSampleData {
    /**
     * All available views
     */
    private static readonly VIEWS: string[] = ["PLANNING", "SCHEDULE", "SEQUENCE", "PRODUCTION", "MURPHY", "COMPLETE"];
    /**
     * Singleton instance
     */
    private static instance: JobSetupEditCriteriaSampleData = null;
    /**
     * Stores the edit view configurations saved by each user.
     */
    private _jobsetupOfUsers: IJobSetupFilterCriterias[] = [];

    private constructor() {
        this._jobsetupOfUsers.push(this.createDefaultFilterCriterias('1'));    
        this._jobsetupOfUsers.push(this.createDefaultFilterCriterias('2'));    
    }
    /**
     * static method to get singleton object
     */
    public static getInstance(): JobSetupEditCriteriaSampleData {
        if (this.instance === null) { 
            this.instance = new JobSetupEditCriteriaSampleData();
        }
        return this.instance;
    }
    /**
     * Creates the default filter criterias for a given user-id.
     * If a user does not have previously saved filter criterias, these default filter criterias will be applied. 
     * @param userId user id to create default filter criterias.
     */
    private createDefaultFilterCriterias(userId: string): IJobSetupFilterCriterias {
        let _jobsetup: IJobSetupFilterCriterias = <IJobSetupFilterCriterias>{};
        _jobsetup.userId = userId;
        // todo GK: implement other methods with default filter criterias.
        _jobsetup.planning = this.getplanning();
        _jobsetup.schedule = this.getplanning();
        _jobsetup.sequence = this.getSequence();
        _jobsetup.production = this.getplanning();
        _jobsetup.murphy = this.getplanning();
        _jobsetup.complete = this.getplanning();
        return _jobsetup;
    }
    /**
     * Returns the matching edit criteria for the given user-id and view
     * @param userId user's id to get saved filter criterias
     * @param view specific view that needs filter criteris of
     */
    public getEditCriteria(userId: string, view: string): ICoreLayoutOptions[] {
        let _layout = this.getAllEditCriteriasByUserId(userId);

        switch (view.toLocaleUpperCase()) {
            case JobSetupEditCriteriaSampleData.VIEWS[0]:
                return _layout.planning;
            case JobSetupEditCriteriaSampleData.VIEWS[1]:
                return _layout.schedule;
            case JobSetupEditCriteriaSampleData.VIEWS[2]:
                return _layout.sequence;
            case JobSetupEditCriteriaSampleData.VIEWS[3]:
                return _layout.production;
            case JobSetupEditCriteriaSampleData.VIEWS[4]:
                return _layout.murphy;
            case JobSetupEditCriteriaSampleData.VIEWS[5]:
                return _layout.complete;
        }

    }
    /**
     * Returns all criterias associated with the given user. 
     * If no previous saved edit criteria is available, default criterias will be returned.
     * @param userId user's id
     */
    public getAllEditCriteriasByUserId(userId: string) {
        let layout: IJobSetupFilterCriterias = <IJobSetupFilterCriterias> {};
        for(layout of this._jobsetupOfUsers) {
            if(layout.userId === userId) {
                return layout;
            }
        }
        // if no filter criteria default setup would be created for the user.
        let newCriteria = this.createDefaultFilterCriterias(userId);
        this._jobsetupOfUsers.push(newCriteria);
        return newCriteria;
    }

    public updateEditSetupInformation(userId: string, view: string, payload: FieldOptions): boolean {
        // todo GK: test functionality with new data
        let __job: IJobSetupFilterCriterias;
        for (__job of this._jobsetupOfUsers) {
            if (__job.userId === userId) {
                __job[view.toLowerCase()] = payload;
                return true;
            }
        }

        return false;
    }

    private getSequence(): ICoreLayoutOptions[] {

        let sequenceList: ICoreLayoutOptions[] = [];

        let layout: ICoreLayoutOptions = <ICoreLayoutOptions>{};
        layout.width = WidthOptions.SINGLE;
        layout.field = FieldOptions.ESTIMATEID;
        layout.filter = true;
        layout.filtervalue = "< 1000";

        let layout1: ICoreLayoutOptions = <ICoreLayoutOptions>{};
        layout1.width = WidthOptions.SINGLE;
        layout1.field = FieldOptions.ESTIMATEID;
        layout1.filter = true;
        layout1.filtervalue = "< 1000";

        sequenceList.push(layout, layout1);

        return sequenceList;
    }

    private getplanning(): IEditLayoutOptions[] {
        let planningList: IEditLayoutOptions[] = [];

        let layout1: IEditLayoutOptions = <IEditLayoutOptions>{};
        layout1.width = WidthOptions.SINGLE;
        layout1.field = FieldOptions.ESTIMATEID;
        layout1.sortPriority = SortPriority.PRIMARY;
        layout1.sortOrder = SortOrder.ASCENDING;
        layout1.filter = false;
        layout1.filtervalue = "< 1000";

        let layout2: IEditLayoutOptions = <IEditLayoutOptions>{};
        layout2.width = WidthOptions.SINGLE;
        layout2.field = FieldOptions.VEHICLETYPE;
        layout2.sortPriority = SortPriority.SECONDARY;
        layout2.sortOrder = SortOrder.DESCENDING;
        layout2.filter = false;
        layout2.filtervalue = null;

        let layout3: IEditLayoutOptions = <IEditLayoutOptions>{};
        layout3.width = WidthOptions.SINGLE;
        layout3.field = FieldOptions.JOBID;
        layout3.sortPriority = SortPriority.SECONDARY;
        layout3.sortOrder = SortOrder.DESCENDING;
        layout3.filter = true;
        layout3.filtervalue = "< JB100";

        planningList.push(layout1, layout2, layout3);

        /*Data.jobSetupList.map((_jsl) => {
            for (let _field of _jsl.setupFields) {
                let layout: IEditLayoutOptions = <IEditLayoutOptions>{};
                layout.width = WidthOptions.SINGLE;
                layout.field = _field.fieldName;
                layout.sortPriority = SortPriority.PRIMARY;
                layout.sortOrder = SortOrder.ASCENDING;
                layout.filter = false;
                layout.filtervalue = "< 1000";

                planningList.push(layout);
            }
        });*/
        

        return planningList;
    }
}