const uuid = require('uuid');

export interface ILevel {
    parent: string,
    data: string[]
};

export interface IAllLists {
    coreItems: string[],
    level1Items: ILevel[],
    level2Items: ILevel[]
}

export class SetupListData {

    private static level0: string[] = ['Vehicle type', 'Murphy continue', 'Murphy delay', 'Murphy reject', 'Delays', 'Completed', 'Check List-x2y', 'Make & Model', 'Engine CC', 'Test List'];
    private static level1: ILevel[] = [
        {parent: 'Vehicle type', data: ['Sedan', 'Coupe', 'Hatchback', 'Convertible']},     
        {parent: 'Murphy continue', data: ['Parts', 'Estimating', 'Color', 'Other']},  
        {parent: 'Murphy delay', data: ['Parts', 'Estimating', 'Color', 'Shipping Delay', 'Not In Production', 'Other']},
        {parent: 'Murphy reject', data: ['Parts', 'Estimating', 'Color', 'Other']},
    ];

    private static level2: ILevel[] = [
        {parent: 'Sedan', data: ['Scoop Lights', 'Round Tail Lights', 'LED Heads']},
        {parent: 'Coupe', data: []},
        {parent: 'Sky Blue', data: []},       
        {parent: 'Parts', data: ['Out of Stock', 'Not available in agents']},
        {parent: 'Shipping Delay', data: ['Lost in shipping', 'In warehouse', ' Other']},
        {parent: 'Color', data: ['Cherry Red', 'Sky Blue', 'Silver', 'Matt Black', 'Other']},
        {parent: 'Estimating', data: ['Wrong Estimations', 'Additional damages discovered', 'Other']},
        {parent: 'Not in Production', data: ['Late delivery of the Vehicle', 'Other']},
        {parent: 'Other', data: ['Other']},
        
    ];

    private static allLists = {
        coreItems: SetupListData.level0,
        level1Items: SetupListData.level1,
        level2Items: SetupListData.level2
    };

    private static getL1DataById(_listId: string) {
        for (let _l1 of SetupListData.level1) {
            if(_l1.parent.toLowerCase() == _listId.toLowerCase()) {
                return _l1;
            }
        }

        return null;
    }

    public static getAllLists() {
        return SetupListData.allLists;
    }

    public static getListById(listId: string) {
        let _listId = listId.toLowerCase();

        for (let l1 of SetupListData.level1) {
            if (l1.parent.toLowerCase() == _listId) {
                /*let result: ILevel = SetupListData.getL1DataById(_listId);
                if (result !== null) {
                    return result.data;
                }*/

                return l1;
            }
        }

        for (let l2 of SetupListData.level2) {
            if (l2.parent.toLowerCase() == _listId) {
                return l2;
            }
        }
    }
}