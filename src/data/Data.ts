const uuid = require('uuid');

export class Data {
    public static userList = [
        {
            userId: '1',
            firstName: 'Lasitha',
            surName: 'Petthawadu',
            username: 'Lasitha',
            siteEmail: 'lasitha.petthawadu@bback.com',
            roles: [
                {
                    roleId: "R001",
                    roleName: "Technician",
                    roleDescription: "Technician Description"
                }
            ],
            active: true,
            temporaryStaff: true,
            contractStartDate: '2016/12/01',
            contractEndDate: '2018/12/31',
            permissionLevel: {
                permissionId: 'PR002',
                permissionName: 'Manager',
            },
            qualifications: [{
                qualificationDescription: "Bsc in Engineering"
            }],
            skill: [
                {
                    skillId: "SK001",
                    skillName: "Technician - A",
                    skillDescription: "A Description"
                }],
            profileImg: 'https://dl.dropboxusercontent.com/u/99892820/male-users/men18-photo-by-Dave-Fayr' +
            'am.jpg',
            password: '$2a$05$8Pxnqsu4OU2ZaNN2gEy2neQm4Xn2QqCosJPL25LyRWrR3tX0TYstO',
            authToken: uuid.v4()
        },
        {
            userId: '2',
            firstName: 'Rasika',
            surName: 'Bandara',
            siteEmail: 'rasikab@bback.com',
            username: 'rasika',
            roles: [
                {
                    roleId: 'R007',
                    roleName: 'Administrator',
                    roleDescription: 'Admin Description'
                }
            ],
            active: true,
            temporaryStaff: false,
            contractStartDate: '2016/12/01',
            contractEndDate: '2017/12/31',
            permissionLevel: {
                permissionId: 'PR001',
                permissionName: 'Admin',
            },
            qualifications: [{
                qualificationDescription: "Bsc in Engineering"
            }],
            skill: [
                {
                    skillId: "SK001",
                    skillName: "Technician - A",
                    skillDescription: "A Description"
                }],
            profileImg: 'https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAKiAAAAJGM3ODE0OTk' +
            'yLTU2YjQtNGU0Yy04MTUwLWVlY2VlODZiOTE5MQ.jpg',
            password: '$2a$05$8Pxnqsu4OU2ZaNN2gEy2neQm4Xn2QqCosJPL25LyRWrR3tX0TYstO',
            authToken: uuid.v4()
        },
        {
            userId: '3',
            firstName: 'Chandima',
            surName: 'Anuradha',
            siteEmail: 'chandimah@bback.com',
            username: 'chandima',
            roles: [{
                roleId: 'R007',
                roleName: 'Administrator',
                roleDescription: 'Admin Description'
            }],
            active: true,
            temporaryStaff: false,
            contractStartDate: '2016/12/01',
            contractEndDate: '2019/12/31',
            permissionLevel: {
                permissionId: 'PR001',
                permissionName: 'Admin',
            },
            qualifications: [{
                qualificationDescription: "Bsc in Engineering"
            }],
            skill: [
                {
                    skillId: "SK001",
                    skillName: "Technician - A",
                    skillDescription: "A Description"
                }],
            profileImg: 'https://lh4.googleusercontent.com/-B3s3KKqlguY/AAAAAAAAAAI/AAAAAAAAAIc/3SOpiGC_F' +
            'hA/photo.jpg',
            password: '$2a$05$8Pxnqsu4OU2ZaNN2gEy2neQm4Xn2QqCosJPL25LyRWrR3tX0TYstO',
            authToken: uuid.v4()
        },
        {
            userId: '4',
            firstName: 'test',
            surName: 'Testing',
            siteEmail: 'test.testing@bback.com',
            username: 'test',
            roles: [
                {
                    roleId: 'R007',
                    roleName: 'Administrator',
                    roleDescription: 'Admin Description'
                }
            ],
            active: true,
            temporaryStaff: true,
            contractStartDate: '2016/12/01',
            contractEndDate: '2016/12/31',
            permissionLevel: {
                permissionId: 'PR001',
                permissionName: 'Admin',
            },
            qualifications: [{
                qualificationDescription: "Bsc in Engineering"
            }],
            skill: [
                {
                    skillId: "SK001",
                    skillName: "Technician - A",
                    skillDescription: "A Description"
                }],
            profileImg: 'https://lh4.googleusercontent.com/-B3s3KKqlguY/AAAAAAAAAAI/AAAAAAAAAIc/3SOpiGC_F' +
            'hA/photo.jpg',
            password: '$2a$05$8Pxnqsu4OU2ZaNN2gEy2neQm4Xn2QqCosJPL25LyRWrR3tX0TYstO',

            authToken: uuid.v4()
        }
    ];

    public static permissionList = [
        {
            permissionId: 'PR001',
            permissionName: 'Admin',
            permissionDescription: 'User has admin privileges',

        }, {
            permissionId: 'PR002',
            permissionName: 'Manager',
            permissionDescription: 'User has manager privileges',
        }, {
            permissionId: 'PR003',
            permissionName: 'Normal',
            permissionDescription: 'User has no spacial privileges',
        }
    ];

    public static jobSetupList = [
        {
            jobSetupId: 1,
            setupFields: [
                {
                    fieldName: 'Job ID',
                    InSequenceTrigger: false,
                    lockedInProduction: true,
                    inputType: {
                        name: 'Alphanumeric',
                        id: 1
                    },
                    inputTypeDetails: {
                        id: 11,
                        name: 'Short'
                    },
                    default: true
                },
                {
                    fieldName: 'Vehicle on Site',
                    InSequenceTrigger: true,
                    lockedInProduction: true,
                    inputType: {
                        name: 'Checkbox',
                        id: 4
                    },
                    inputTypeDetails: {
                        id: 41,
                        name: 'Default Off'
                    },
                    default: true
                },
                {
                    fieldName: 'Estimate ID',
                    InSequenceTrigger: false,
                    lockedInProduction: true,
                    inputType: { 
                        name: 'Alphanumeric',
                         id: 1 
                    },
                    inputTypeDetails: { 
                        id: 11, 
                        name: 'Short' 
                    },
                    default: true
                }
            ]
        }
    ]

    public static skillList = [
        {
            skillId: 'SK001',
            skillName: 'A',
            skillDescription: 'A Description',
            efficiencyPercent: '20%',
            efficiencyHrs: ''

        }, {
            skillId: 'SK002',
            skillName: 'B',
            skillDescription: 'B Description',
            efficiencyPercent: '10%',
            efficiencyHrs: ''
        }, {
            skillId: 'SK003',
            skillName: 'C',
            skillDescription: 'C Description',
            efficiencyPercent: '10%',
            efficiencyHrs: '14',

        }
    ];

    public static roleList = [
        {
            roleId: 'R001',
            roleName: 'Technician',
            roleDescription: 'Technician Description'
        }, {
            roleId: 'R003',
            roleName: 'Painter',
            roleDescription: 'Painter Description'
        }, {
            roleId: 'R004',
            roleName: 'Floater',
            roleDescription: 'Floater Description'
        }, {
            roleId: 'R005',
            roleName: 'Specialist',
            roleDescription: 'Specialist Description'
        }, {
            roleId: 'R006',
            roleName: 'Manager',
            roleDescription: 'Manager Description'
        }, {
            roleId: 'R007',
            roleName: 'Administrator',
            roleDescription: 'Admin Description'
        }
    ];

    public static leaves = [
        {
            leaveId: 'L001',
            userId: '1',
            startDate: '2017/01/25',
            endDate: '2017/01/25',
            reason: 'Sick with fever',
            status: 'pending'
        }, {
            leaveId: 'L002',
            userId: '1',
            startDate: '2017/01/20',
            endDate: '2017/01/20',
            reason: 'Vacation for shopping',
            status: 'rejected'
        }, {
            leaveId: 'L003',
            userId: '2',
            startDate: '2017/01/21',
            endDate: '2017/01/21',
            reason: 'Trip to Dubai',
            status: 'approved'
        }
    ];

    public static departments = [
        {
            departmentId: 'Dept001',
            departmentName: 'Workshop',
            setup: {
                deptBufferTime: '8',
                deptBufferPercent: '',
                otherBufferTime: '4',
                otherBufferPercent: '',
                isProduction: true,
                isOutwork: true
            },
            checkpoints: { "1": ["CHK003", "clocking on"], "2": ["CHK004", "clocking off"] },
            checkpointOrder: ["1", "2"]
        }, {
            departmentId: 'Dept002',
            departmentName: 'Paintshop',
            setup: {
                deptBufferTime: '4',
                deptBufferPercent: '',
                otherBufferTime: '2',
                otherBufferPercent: '',
                isProduction: true,
                isOutwork: true
            },
            checkpoints: { "1": ["CHK003", "clocking on"], "2": ["CHK004", "clocking off"] },
            checkpointOrder: ["1", "2"]
        }, {
            departmentId: 'Dept003',
            departmentName: 'QC',
            setup: {
                deptBufferTime: '4',
                deptBufferPercent: '',
                otherBufferTime: '2',
                otherBufferPercent: '',
                isProduction: true,
                isOutwork: true
            },
            checkpoints: { "1": ["CHK003", "clocking on"], "2": ["CHK004", "clocking off"] },
            checkpointOrder: ["1", "2"]
        }, {
            departmentId: "Dept006",
            departmentName: "Mechanical",
            setup: {
                deptBufferTime: "",
                deptBufferPercent: "10",
                otherBufferTime: "",
                otherBufferPercent: "",
                isProduction: true,
                isOutwork: false
            },
            checkpoints: { "1": ["CHK001", "Clocking Off"], "2": ["CHK002", "Clocking On"] },
            checkpointOrder: ["1", "2"]
        }
    ];

    public static checklists = [
        {
            checkListId: 'CHK001',
            checkListName: 'Workflow start CheckList',
            items: {
                '1': {
                    itemId: '1',
                    itemName: 'Item 1',
                    type: 'photo'
                },
                '2': {
                    itemId: '2',
                    itemName: 'Item 2',
                    type: 'checkbox'
                },
                '3': {
                    itemId: '3',
                    itemName: 'Item 3',
                    type: 'photo'
                },
                '4': {
                    itemId: '4',
                    itemName: 'Item 4',
                    type: 'checkbox'
                }
            },
            checkListOrder: ['1', '2', '3', '4']
        }, {
            checkListId: 'CHK002',
            checkListName: 'Workflow End CheckList',
            items: {
                '1': {
                    itemId: '1',
                    itemName: 'Paintshop Item 1',
                    type: 'photo'
                },
                '2': {
                    itemId: '2',
                    itemName: 'Paintshop Item 2',
                    type: 'checkbox'
                }
            },
            checkListOrder: ['1', '2']
        }, {
            checkListId: 'CHK003',
            checkListName: 'Department Start checklist',
            items: {
                '1': {
                    itemId: '1',
                    itemName: 'Item 1',
                    type: 'checkbox'
                },
                '2': {
                    itemId: '2',
                    itemName: 'Item 2',
                    type: 'photo'
                }
            },
            checkListOrder: ['1', '2']
        }, {
            checkListId: 'CHK004',
            checkListName: 'Department End checklist',
            items: {
                '1': {
                    itemId: '1',
                    itemName: 'Item 1',
                    type: 'checkbox'
                },
                '2': {
                    itemId: '2',
                    itemName: 'Item 2',
                    type: 'photo'
                }
            },
            checkListOrder: ['1', '2']
        }, {
            checkListId: 'CHK005',
            checkListName: 'Clean Workbay Checklist',
            items: {
                '1': {
                    itemId: '1',
                    itemName: 'Item 1',
                    type: 'checkbox'
                },
                '2': {
                    itemId: '2',
                    itemName: 'Item 2',
                    type: 'photo'
                },
                '3': {
                    itemId: '3',
                    itemName: 'Item 3',
                    type: 'checkbox'
                }
            },
            checkListOrder: ['1', '2']
        }
    ];


    public static workflows = [
        {
            workflowId: "WF001",
            workflowName: "Bodyshop Revolution 1",
            SkillDept: [
                { skillId: "SK001", departmentId: "Dept001" },
                { skillId: "SK002", departmentId: "Dept001" },
                { skillId: "SK003", departmentId: "Dept003" }
            ],
            TechOpen: [
                { departmentId: 'Dept001', open: ['Dept002'] },
                { departmentId: 'Dept002', open: ['Dept001', 'Dept003'] },
            ],
            Buffers: { fixedTime: '', Efficiency: '20' },
            checkpoints: {
                '1': ['CHK001', 'Starting Job'],
                '2': ['CHK002', 'Completing Job']
            },
            checkpointOrder: ['1', '2']
        }, {
            workflowId: "WF002",
            workflowName: "Traditional 1",
            SkillDept: [
                { skillId: "SK001", departmentId: "Dept002" },
                { skillId: "SK002", departmentId: "Dept001" },
                { skillId: "SK003", departmentId: "Dept002" }
            ],
            TechOpen: [
                { departmentId: 'Dept001', open: ['Dept002', 'Dept003'] },
                { departmentId: 'Dept002', open: ['Dept001', 'Dept003'] },
            ],
            Buffers: { fixedTime: '', Efficiency: '20' },
            checkpoints: {
                '1': ['CHK001', 'Starting Job']
            },
            checkpointOrder: ['1']
        }, {
            workflowId: "WF003",
            workflowName: "Bodyshop Revolution 2",
            SkillDept: [],
            TechOpen: [],
            Buffers: {},
            checkpoints: {},
            checkpointOrder: []
        },
    ];

    public static jobs = [
        {
            jobID: 'JB001',
            createDate: '',
            estimatedCompletionDate: '',
            jobInfo: [
                [
                    {
                        fieldName: 'Job ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: { name: 'Alphanumeric', id: 1 },
                        inputTypeDetails: { id: 11, name: 'Short' },
                        default: true
                    }, 
                    'JB001'
                ],[
                    {
                        fieldName: 'Estimate ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: { name: 'Alphanumeric', id: 1 },
                        inputTypeDetails: { id: 11, name: 'Short' },
                        default: true
                    }, 
                    'ET001'
                ], [
                    {
                        fieldName: 'Vehicle on Site',
                        InSequenceTrigger: true,
                        lockedInProduction: true,
                        inputType: { name: 'Checkbox', id: 4 },
                        inputTypeDetails: { id: 41, name: 'Default Off' },
                        default: true
                    },
                    true
                ]
            ],

            notes: '',
            jobEvents: [
                    {
                        EventName: 'Starting Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''

                    },
                    {
                        EventName: 'Completing Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''
                        
                    }
            ],
            workflowID: '',
            jobLineList: {
                "1": ["1.0", "Strip", "3", "", "SK001", "", "1", "I"],
                "2": ["2.0", "Pull Dents", "4", "", "SK002", "", "2", "I"],
                "3": ["3.0", "Filler", "1", "", "SK002", "", "3", "I"],
                "4": ["4.0", "Primer", "1", "", "SK003", "", "4", "I"],
                "5": ["5.0", "Paint", "3", "", "SK001", "", "5", "I"],
                "6": ["6.0", "Polish", "1", "", "SK001", "", "6", "I"],
                "7": ["7.0", "Refit", "2", "", "SK003", "", "7", "I"]
            },
            order: ['1', '2', '3', '4', '5', '6', '7'],
            customList: [
                ["Custom-CL", "CHK002", "", "", "", "", "", "C"],
                ["Custom-CL", "CHK004", "", "", "", "", "", "C"],
                ["Custom-CL", "CHK003", "", "", "", "", "", "C"]
            ],
            completedCheklistItems: {
                '12': [['1'], false],
                '16': [['1', '2'], false],
            },
            jobProgress: {
                currentDepartment: ['', '', '', ''],            // [department Id, department group, currentDepartmentPlannedHrs, currentUser]
                currentSectionId: '',
                progress: [],
                workflowFinishDate: ''
            },
            attachments: {
                file: [],
                photo: []
            },
            murphy: {
                currentState: ['', '', ''],             // [murphyType, timeStamp, reason]
                Continue: [],
                Delay: [],
                Remove: []
            },
            sections: []        
            /*sections object contains array objects with
                     [
                                [groupofItems], 
                 *              [skillset], 
                 *              totalsctionhrs, 
                 *              noOfDepartments in the section,
                 *              sectionUser 
                 *              sectionStart Timestamp, 
                 *              section End Timestamp,
                 *              sectionId
                 * ]
                 **/
        },
        {
            jobID: 'JB002',
            createDate: '',
            estimatedCompletionDate: '',
            jobInfo: [
                [
                    {
                        fieldName: 'Job ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'JB002'
                ], [
                    {
                        fieldName: 'Estimate ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'ET002'
                ],
                [
                    {
                        fieldName: 'Vehicle on Site',
                        InSequenceTrigger: true,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Checkbox',
                            id: 4
                        },
                        inputTypeDetails: {
                            id: 41,
                            name: 'Default Off'
                        },
                        default: true
                    },
                    true
                ]
            ],
            notes: '',
            jobEvents: [
                    {
                        EventName: 'Starting Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''

                    },
                    {
                        EventName: 'Completing Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''
                        
                    }
            ],
            workflowID: '',
            jobLineList: {
                "1": [
                    "1.0",
                    "Strip",
                    "3",
                    "",
                    "SK001",
                    "",
                    "1",
                    "I"
                ],
                "2": [
                    "2.0",
                    "Pull Dents",
                    "4",
                    "",
                    "SK002",
                    "",
                    "2",
                    "I"
                ],
                "3": [
                    "3.0",
                    "Filler",
                    "1",
                    "",
                    "SK002",
                    "",
                    "3",
                    "I"
                ],
                "4": [
                    "4.0",
                    "Primer",
                    "1",
                    "",
                    "SK003",
                    "",
                    "4",
                    "I"
                ],
                "5": [
                    "5.0",
                    "Paint",
                    "3",
                    "",
                    "SK001",
                    "",
                    "5",
                    "I"
                ],
                "6": [
                    "6.0",
                    "Polish",
                    "1",
                    "",
                    "SK001",
                    "",
                    "6",
                    "I"
                ],
                "7": [
                    "7.0",
                    "Refit",
                    "2",
                    "",
                    "SK003",
                    "",
                    "7",
                    "I"
                ]
            },
            order: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7'
            ],
            customList: [
                [
                    "Custom-CL",
                    "CHK002",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK004",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK003",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ]
            ],
            completedCheklistItems: {
                '12': [
                    [
                        '1'
                    ],
                    false
                ],
                '16': [
                    [
                        '1',
                        '2'
                    ],
                    false
                ],

            },
            jobProgress: {
                currentDepartment: [
                    '',
                    '',
                    '',
                    ''
                ],
                currentSectionId: '',
                progress: [
                ],
                workflowFinishDate: ''
            },
            attachments: {
                file: [

                ],
                photo: [

                ]
            },
            murphy: {
                currentState: [
                    '',
                    '',
                    ''
                ],
                Continue: [

                ],
                Delay: [

                ],
                Remove: [

                ]
            },
            sections: []
        },
        {
            jobID: 'JB003',
            createDate: '',
            estimatedCompletionDate: '',
            jobInfo: [
                [
                    {
                        fieldName: 'Job ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'JB003'
                ], [
                    {
                        fieldName: 'Estimate ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'ET003'
                ],
                [
                    {
                        fieldName: 'Vehicle on Site',
                        InSequenceTrigger: true,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Checkbox',
                            id: 4
                        },
                        inputTypeDetails: {
                            id: 41,
                            name: 'Default Off'
                        },
                        default: true
                    },
                    true
                ]
            ],
            notes: '',
            jobEvents: [
                    {
                        EventName: 'Starting Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''

                    },
                    {
                        EventName: 'Completing Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''
                        
                    }
            ],
            workflowID: '',
            jobLineList: {
                "1": [
                    "1.0",
                    "Strip",
                    "3",
                    "",
                    "SK001",
                    "",
                    "1",
                    "I"
                ],
                "2": [
                    "2.0",
                    "Pull Dents",
                    "4",
                    "",
                    "SK002",
                    "",
                    "2",
                    "I"
                ],
                "3": [
                    "3.0",
                    "Filler",
                    "1",
                    "",
                    "SK002",
                    "",
                    "3",
                    "I"
                ],
                "4": [
                    "4.0",
                    "Primer",
                    "1",
                    "",
                    "SK003",
                    "",
                    "4",
                    "I"
                ],
                "5": [
                    "5.0",
                    "Paint",
                    "3",
                    "",
                    "SK001",
                    "",
                    "5",
                    "I"
                ],
                "6": [
                    "6.0",
                    "Polish",
                    "1",
                    "",
                    "SK001",
                    "",
                    "6",
                    "I"
                ],
                "7": [
                    "7.0",
                    "Refit",
                    "2",
                    "",
                    "SK003",
                    "",
                    "7",
                    "I"
                ]
            },
            order: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7'
            ],
            customList: [
                [
                    "Custom-CL",
                    "CHK002",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK004",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK003",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ]
            ],
            completedCheklistItems: {
                '12': [
                    [
                        '1'
                    ],
                    false
                ],
                '16': [
                    [
                        '1',
                        '2'
                    ],
                    false
                ],

            },
            jobProgress: {
                currentDepartment: [
                    '',
                    '',
                    '',
                    ''
                ],
                currentSectionId: '',
                progress: [

                ],
                workflowFinishDate: ''
            },
            attachments: {
                file: [

                ],
                photo: [

                ]
            },
            murphy: {
                currentState: [
                    '',
                    '',
                    ''
                ],
                Continue: [

                ],
                Delay: [

                ],
                Remove: [

                ]
            },
            sections: []
        },
        {
            jobID: 'JB004',
            createDate: '',
            estimatedCompletionDate: '',            
            jobInfo: [
                [
                    {
                        fieldName: 'Job ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'JB004'
                ], [
                    {
                        fieldName: 'Estimate ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'ET004'
                ],
                [
                    {
                        fieldName: 'Vehicle on Site',
                        InSequenceTrigger: true,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Checkbox',
                            id: 4
                        },
                        inputTypeDetails: {
                            id: 41,
                            name: 'Default Off'
                        },
                        default: true
                    },
                    true
                ]
            ],
            notes: '',
            jobEvents: [
                 {
                        EventName: 'Starting Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''

                    },
                    {
                        EventName: 'Completing Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''
                        
                    }
            ],
            workflowID: '',
            jobLineList: {
                "1": [
                    "1.0",
                    "Strip",
                    "3",
                    "",
                    "SK001",
                    "",
                    "1",
                    "I"
                ],
                "2": [
                    "2.0",
                    "Pull Dents",
                    "4",
                    "",
                    "SK002",
                    "",
                    "2",
                    "I"
                ],
                "3": [
                    "3.0",
                    "Filler",
                    "1",
                    "",
                    "SK002",
                    "",
                    "3",
                    "I"
                ],
                "4": [
                    "4.0",
                    "Primer",
                    "1",
                    "",
                    "SK003",
                    "",
                    "4",
                    "I"
                ],
                "5": [
                    "5.0",
                    "Paint",
                    "3",
                    "",
                    "SK001",
                    "",
                    "5",
                    "I"
                ],
                "6": [
                    "6.0",
                    "Polish",
                    "1",
                    "",
                    "SK001",
                    "",
                    "6",
                    "I"
                ],
                "7": [
                    "7.0",
                    "Refit",
                    "2",
                    "",
                    "SK003",
                    "",
                    "7",
                    "I"
                ]
            },
            order: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7'
            ],
            customList: [
                [
                    "Custom-CL",
                    "CHK002",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK004",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK003",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ]
            ],
            completedCheklistItems: {
                '12': [
                    [
                        '1'
                    ],
                    false
                ],
                '16': [
                    [
                        '1',
                        '2'
                    ],
                    false
                ],

            },
            jobProgress: {
                currentDepartment: [
                    '',
                    '',
                    '',
                    ''
                ],
                currentSectionId: '',
                progress: [

                ],
                workflowFinishDate: ''
            },
            attachments: {
                file: [

                ],
                photo: [

                ]
            },
            murphy: {
                currentState: [
                    '',
                    '',
                    ''
                ],
                Continue: [

                ],
                Delay: [

                ],
                Remove: [

                ]
            },
            sections: []
        },
        {
            jobID: 'JB005',
            createDate: '',
            estimatedCompletionDate: '',
            jobInfo: [
                [
                    {
                        fieldName: 'Job ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'JB005'
                ], [
                    {
                        fieldName: 'Estimate ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'ET005'
                ],
                [
                    {
                        fieldName: 'Vehicle on Site',
                        InSequenceTrigger: true,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Checkbox',
                            id: 4
                        },
                        inputTypeDetails: {
                            id: 41,
                            name: 'Default Off'
                        },
                        default: true
                    },
                    true
                ]
            ],
            notes: '',
            jobEvents: [
                {
                        EventName: 'Starting Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''

                    },
                    {
                        EventName: 'Completing Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''
                        
                    }
            ],
            workflowID: '',
            jobLineList: {
                "1": [
                    "1.0",
                    "Strip",
                    "3",
                    "",
                    "SK001",
                    "",
                    "1",
                    "I"
                ],
                "2": [
                    "2.0",
                    "Pull Dents",
                    "4",
                    "",
                    "SK002",
                    "",
                    "2",
                    "I"
                ],
                "3": [
                    "3.0",
                    "Filler",
                    "1",
                    "",
                    "SK002",
                    "",
                    "3",
                    "I"
                ],
                "4": [
                    "4.0",
                    "Primer",
                    "1",
                    "",
                    "SK003",
                    "",
                    "4",
                    "I"
                ],
                "5": [
                    "5.0",
                    "Paint",
                    "3",
                    "",
                    "SK001",
                    "",
                    "5",
                    "I"
                ],
                "6": [
                    "6.0",
                    "Polish",
                    "1",
                    "",
                    "SK001",
                    "",
                    "6",
                    "I"
                ],
                "7": [
                    "7.0",
                    "Refit",
                    "2",
                    "",
                    "SK003",
                    "",
                    "7",
                    "I"
                ]
            },
            order: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7'
            ],
            customList: [
                [
                    "Custom-CL",
                    "CHK002",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK004",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK003",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ]
            ],
            completedCheklistItems: {
                '12': [
                    [
                        '1'
                    ],
                    false
                ],
                '16': [
                    [
                        '1',
                        '2'
                    ],
                    false
                ],

            },
            jobProgress: {
                currentDepartment: [
                    '',
                    '',
                    '',
                    ''
                ],
                currentSectionId: '',
                progress: [

                ],
                workflowFinishDate: ''
            },
            attachments: {
                file: [

                ],
                photo: [

                ]
            },
            murphy: {
                currentState: [
                    '',
                    '',
                    ''
                ],
                Continue: [

                ],
                Delay: [

                ],
                Remove: [

                ]
            },
            sections: []
        },
        {
            jobID: 'JB006',
            createDate: '',
            estimatedCompletionDate: '',
            jobInfo: [
                [
                    {
                        fieldName: 'Job ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'JB006'
                ], [
                    {
                        fieldName: 'Estimate ID',
                        InSequenceTrigger: false,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Alphanumeric',
                            id: 1
                        },
                        inputTypeDetails: {
                            id: 11,
                            name: 'Short'
                        },
                        default: true
                    },
                    'ET006'
                ],
                [
                    {
                        fieldName: 'Vehicle on Site',
                        InSequenceTrigger: true,
                        lockedInProduction: true,
                        inputType: {
                            name: 'Checkbox',
                            id: 4
                        },
                        inputTypeDetails: {
                            id: 41,
                            name: 'Default Off'
                        },
                        default: true
                    },
                    true
                ]
            ],
            notes: '',
            jobEvents: [
                    {
                        EventName: 'Starting Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''

                    },
                    {
                        EventName: 'Completing Job',
                        Scheduled: '',
                        inSequence: '',
                        inProduction: '',
                        inCompletion: ''
                        
                    }
            ],
            workflowID: '',
            jobLineList: {
                "1": [
                    "1.0",
                    "Strip",
                    "3",
                    "",
                    "SK001",
                    "",
                    "1",
                    "I"
                ],
                "2": [
                    "2.0",
                    "Pull Dents",
                    "4",
                    "",
                    "SK002",
                    "",
                    "2",
                    "I"
                ],
                "3": [
                    "3.0",
                    "Filler",
                    "1",
                    "",
                    "SK002",
                    "",
                    "3",
                    "I"
                ],
                "4": [
                    "4.0",
                    "Primer",
                    "1",
                    "",
                    "SK003",
                    "",
                    "4",
                    "I"
                ],
                "5": [
                    "5.0",
                    "Paint",
                    "3",
                    "",
                    "SK001",
                    "",
                    "5",
                    "I"
                ],
                "6": [
                    "6.0",
                    "Polish",
                    "1",
                    "",
                    "SK001",
                    "",
                    "6",
                    "I"
                ],
                "7": [
                    "7.0",
                    "Refit",
                    "2",
                    "",
                    "SK003",
                    "",
                    "7",
                    "I"
                ]
            },
            order: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7'
            ],
            customList: [
                [
                    "Custom-CL",
                    "CHK002",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK004",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ],
                [
                    "Custom-CL",
                    "CHK003",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "C"
                ]
            ],
            completedCheklistItems: {
                '12': [
                    [
                        '1'
                    ],
                    false
                ],
                '16': [
                    [
                        '1',
                        '2'
                    ],
                    false
                ],

            },
            jobProgress: {
                currentDepartment: [
                    '',
                    '',
                    '',
                    ''
                ],
                currentSectionId: '',
                progress: [

                ],
                workflowFinishDate: ''
            },
            attachments: {
                file: [

                ],
                photo: [

                ]
            },
            murphy: {
                currentState: [
                    '',
                    '',
                    ''
                ],
                Continue: [

                ],
                Delay: [

                ],
                Remove: [

                ]
            },
            sections: []
        }

    ];

    //-------------------Start Sprint Data----------------------------------

    public static sprints = [
        {
            sprintID: '1',
            userId: '3',
            jobID: 'JB001',
            sprintType: 'cleaning',
            timeLogged: ['', 2.5],
            checklist: [['1'], false],
        }
    ]

    //-------------------End Sprint Data------------------------------------

    //-------------- Start Calendar Roles Data ---------------------------------
    public static usersToPick = [
        {
            userId: '1',
            firstName: 'Lasitha',
            lastName: 'Petthawadu',
            siteEmail: 'lasitha.petthawadu@bback.com',
            departmentId: 'Dept003'
        },
        {
            userId: '2',
            firstName: 'Rasika',
            lastName: 'Bandara',
            siteEmail: 'rasikab@bback.com',
            departmentId: 'Dept003'
        }, {
            userId: '3',
            firstName: 'Chandima',
            lastName: 'Anuradha',
            siteEmail: 'chandimah@bback.com',
            departmentId: 'Dept004'
        },
        {
            userId: '4',
            firstName: 'test',
            lastName: 'Testing',
            siteEmail: 'test.testing@bback.com',
            departmentId: 'Dept004'
        }
    ];

    public static rolesToPick = [
        {
            roleId: "R001",
            roleName: "Technician"
        },
        {
            roleId: "R003",
            roleName: "Painter"
        },
        {
            roleId: "R004",
            roleName: "Floater"
        },
        {
            roleId: "R005",
            roleName: "Specialist"
        },
        {
            roleId: "R006",
            roleName: "Manager"
        },
        {
            roleId: "R007",
            roleName: "Administrator"
        }
    ];

    public static shiftTimes = [
        {
            shiftStart: '8.00',
            shiftEnd: '9.00'
        }, {
            shiftStart: '9.00',
            shiftEnd: '10.00'
        }, {
            shiftStart: '10.00',
            shiftEnd: '11.00'
        }, {
            shiftStart: '11.00',
            shiftEnd: '12.00'
        }, {
            shiftStart: '12.00',
            shiftEnd: '1.00'
        }, {
            shiftStart: '1.00',
            shiftEnd: '2.00'
        }, {
            shiftStart: '2.00',
            shiftEnd: '3.00'
        }, {
            shiftStart: '3.00',
            shiftEnd: '4.00'
        }, {
            shiftStart: '4.00',
            shiftEnd: '5.00'
        }
    ];
    //-------------- End Calendar Roles Data -----------------------------------


    public static userStateList = [
        {
            userId: 1,
            calendaredOff: false,
            onBreak: false,
            state: 'PRODUCTIVE',
            timestamp: "2017-03-23T01:00:29.948Z",
            previous: null
        }
    ]


    public static patternData = [
        {
            startDate: "2017/03/06",
            endDate: "2017/03/11",
            userId: "2",
            userName: "Rasika Bandara",
            rolePattern: [
                {
                    day: "MONDAY",
                    date: "2017/03/06",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "8.00",
                    endTime: "13.00"
                },
                {
                    day: "TUESDAY",
                    date: "2017/03/07",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "10.00",
                    endTime: "13.00"
                },
                {
                    day: "WEDNESSDAY",
                    date: "2017/03/08",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "12.00",
                    endTime: "13.00"
                },
                {
                    day: "THURSDAY",
                    date: "2017/03/09",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "12.00",
                    endTime: "13.00"
                },
                {
                    day: "FRIDAY",
                    date: "2017/03/10",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "12.00",
                    endTime: "13.00"
                },
                {
                    day: "SATURDAY",
                    date: "2017/03/11",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "12.00",
                    endTime: "13.00"
                }
            ]
        }, {
            startDate: "2017/03/06",
            endDate: "2017/03/11",
            userId: "1",
            userName: "Lasitha Pettawadu",
            rolePattern: [
                {
                    day: "MONDAY",
                    date: "2017/03/06",
                    roleId: "R003",
                    role: "Painter",
                    department: "Dept003",
                    startTime: "10.00",
                    endTime: "13.00"
                },
                {
                    day: "TUESDAY",
                    date: "2017/03/07",
                    roleId: "R001",
                    role: "Technician",
                    department: "Dept001",
                    startTime: "10.00",
                    endTime: "13.00"
                },
                {
                    day: "WEDNESSDAY",
                    date: "2017/03/08",
                    roleId: "R004",
                    role: "Floater",
                    department: "Dept004",
                    startTime: "10.00",
                    endTime: "13.00"
                },
                {
                    day: "THURSDAY",
                    date: "2017/03/09",
                    roleId: "R003",
                    role: "Painter",
                    department: "Dept003",
                    startTime: "10.00",
                    endTime: "13.00"
                },
                {
                    day: "FRIDAY",
                    date: "2017/03/10",
                    roleId: "R003",
                    role: "Painter",
                    department: "Dept003",
                    startTime: "10.00",
                    endTime: "13.00"
                },
                {
                    day: "SATURDAY",
                    date: "2017/03/11",
                    roleId: "R003",
                    role: "Painter",
                    department: "Dept003",
                    startTime: "10.00",
                    endTime: "13.00"
                }
            ]
        }
    ];

   //-----------------------------User Allocation Data--------------------------------------------
    public static userAllocation = [
        {
            userId:'1',
            jobID:'JB001',
            sectionId:'1',
            jobStatus:'0', 
            startedTime:'',
            completedTime:'',
            techFreeTime: '2017-04-07T18:30:00.000Z'

        }
    ];

    public static pendingJobs = [
        {
            jobID:'1',
            isProduction:'1',
            section:[],
            workflowID:'',
            isAllocated:'0',
            ScheduledStartDate :'2017-04-07T18:30:00.000Z',
            plannedCompletedDate : '2017-04-07T18:30:00.000Z',
            isStarted:'0'
        }
    ];


    public static techFreeTime = [
        {
            techFreeTime:'2017-04-07T18:30:00.000Z',
            userId:'1',
            skill:[],
            isAllocated:'1',
            timeRemaning:'15',
            isStarted:'0',
        }
    ]; 
   //-----------------------------End User Allocation Data----------------------------------------- 
   

}