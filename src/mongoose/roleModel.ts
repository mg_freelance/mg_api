var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const RoleSchema = new Schema({
    roleId: String,
    roleName: String,
    roleDescription: String,
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const Role = mongoose.model('Role', RoleSchema);
export {Role}