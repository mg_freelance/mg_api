var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const LoginUserSchema = new Schema({
    userId: String,
    customerUserId: String,
    firstName: String,
    surName: String,
    username: String,
    siteEmail: String,
    roles: [{ roleId: String, roleName: String, roleDescription: String }],
    active: Boolean,
    temporaryStaff: Boolean,
    contractStartDate: String,
    contractEndDate: String,
    permissionLevel: { permissionId: String, permissionName: String },
    qualifications: [{ qualificationDescription: String }],
    skill: [{ skillId: String, skillName: String, skillDescription: String }],
    profileImg: String,
    password: String,
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const LoginUser = mongoose.model('LoginUser', LoginUserSchema);
export { LoginUser }