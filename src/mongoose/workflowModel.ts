var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const WorkflowSchema = new Schema({
    workflowId: String,
    workflowName: String,
    SkillDept: [{ skillId: String, departmentId: String }],
    TechOpen: [{ departmentId: String, open: [String] }],
    Buffers: { fixedTime: String, Efficiency: String },
    checkpoints: String, // TOBE REVISED
    checkpointOrder: [String],
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const Workflow = mongoose.model('Workflow', WorkflowSchema);
export {Workflow}


/*

Changes
=======

checkpoints: {
    '1': ['CHK001', 'Starting Job'],
    '2': ['CHK002', 'Completing Job']
},

*/