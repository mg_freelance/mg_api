var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SprintSchema = new Schema({
    sprintID: String,
    userId: String,
    jobID: String,
    sprintType: String,
    timeLogged: [],
    checklist: {
        done: [String],
        isCompleted: Boolean
    },
});

const Sprint = mongoose.model('Sprint', SprintSchema);
export {Sprint}