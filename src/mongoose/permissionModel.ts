var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const PermissionSchema = new Schema({
    permissionId: String,
    permissionName: String,
    permissionDescription: String
});

const Permission = mongoose.model('Permission', PermissionSchema);
export {Permission}