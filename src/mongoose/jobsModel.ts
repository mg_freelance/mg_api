var mongoose = require('mongoose');
const Schema = mongoose.Schema;

// const SingleJobSchema = new Schema({
//     singleJobId: String,
//     fieldName: String,
//     InSequenceTrigger: Boolean,
//     lockedInProduction: Boolean,
//     inputType: { name: String, id: Number },
//     inputTypeDetails: { id: Number, name: String },
//     default: true
// })

const JobsSchema = new Schema({
    jobID: String,
    createDate: String,
    estimatedCompletionDate: String,
// <<<<<<< HEAD
//     jobInfo: [Schema.Types.Mixed], // TO BE REVISED
// =======
    jobInfo: [], // TO BE REVISED
    notes: [
        {
            timeStamp: String,
            username: String,
            content: String
        }
    ],
    jobEvents: [
            {
                EventName: String,
                Scheduled: String,
                inSequence: String,
                inProduction: String,
                inCompletion: String
            }
    ],
    workflowID: String,
    jobLineList: String, // TOBE REVISED
    order: [String],
    customList: [],
    completedCheklistItems: String, // THIS IS A OBJECT. But saved as a string
    jobProgress: {
        currentDepartment: [String],            
        currentSectionId: String,
        progress: [],
        workflowFinishDate: String
    },
    attachments: {
        file: [],
        photo: []
    },
    murphy: {
        currentState: [String],
        Continue: [],
        Delay: [],
        Remove: []
    },
    sections: [],
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const Job = mongoose.model('Job', JobsSchema);
export {Job}


/*

LOTS OF CHANGES HERE, CHECK Data.ts

*/