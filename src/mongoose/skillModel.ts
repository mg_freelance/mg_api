var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SkillSchema = new Schema({
    skillId: String,
    skillName: String,
    skillDescription: String,
    efficiencyPercent: String, // TODO: THIS IS A NUMBER
    efficiencyHrs: String, // TODO: THIS IS A NUMBER
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const Skill = mongoose.model('Skill', SkillSchema);
export {Skill} ;