var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ChecklistSchema = new Schema({
    checkListId: String,
    checkListName: String,
    items: String, // TOBE REVISED
    checkListOrder: [String],
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const Checklist = mongoose.model('Checklist', ChecklistSchema);
export {Checklist} ;

/*

Changes
=======

items: {
    '1': {
        itemId: '1',
        itemName: 'Item 1',
        type: 'photo'
    },
    '2': {
        itemId: '2',
        itemName: 'Item 2',
        type: 'checkbox'
    }
}

*/