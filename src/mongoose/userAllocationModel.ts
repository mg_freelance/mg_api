var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const UserAllocationSchema = new Schema({
    userId:String,
    jobID:String,
    sectionId:String,
    jobStatus:String, 
    startedTime:String,
    completedTime:String,
    techFreeTime: { type: Date , Default: Date.now }
});

const UserAllocation = mongoose.model('UserAllocation', UserAllocationSchema);
export {UserAllocation}