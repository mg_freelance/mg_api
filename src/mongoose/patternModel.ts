var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const PatternSchema = new Schema({
    startDate: String,
    endDate: String,
    userId: String,
    userName: String,
    rolePattern: [
        {
            day: String,
            date: String,
            roleId: String,
            role: String,
            department: String,
            startTime: String,
            endTime: String
        }
    ]
});

const CalendarPattern = mongoose.model('Pattern', PatternSchema);
export {CalendarPattern}