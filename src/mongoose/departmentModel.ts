var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const DepartmentSchema = new Schema({
    departmentId: String,
    departmentName: String,
    setup: { 
        deptBufferTime: String,  
        deptBufferPercent: String,
        otherBufferTime: String,
        otherBufferPercent: String,
        isProduction: Boolean,
        isOutwork: Boolean
    },
    checkpoints: String, // changed here. This is a object. But string
    checkpointOrder: [String],
    createdDate:String,
    createdBy: String,
    modifiedDate: String,
    modifiedBy : String,
    deletedDate: String,
    deletedBy: String,
    isDeleted: {type:Boolean , default: false}
});

const Department = mongoose.model('Department', DepartmentSchema);
export {Department}

/*

Changes
=======

checkpoints: {
     "1": ["CHK003", "clocking on"], 
     "2": ["CHK004", "clocking off"] 
},

*/