var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const JobFieldsSetupSchema = new Schema({
    jobSetupId: Number,
    setupFields: [{
        fieldId: String,
        fieldName: String,
        InSequenceTrigger: Boolean,
        lockedInProduction: Boolean,
        inputType: { name: String, id: Number },
        inputTypeDetails: { name: String, id: Number },
        default: Boolean,
        tab: String,
        color: { type: String, default: null },
        warnUser: { type: Boolean, default: false },
        warnText: { type: String, default: null },
        overruledMessage: { type: Boolean, default: false }
    }],
    tabs: [],
    markers: []
});

const JobFieldsSetup = mongoose.model('JobFieldsSetup', JobFieldsSetupSchema);
export {JobFieldsSetup}
