var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const UserStateSchema = new Schema({
    userId: String,
    calendaredOff: { type: Boolean, default: false },
    onBreak: { type: Boolean, default: false},
    state: { type: String, default: 'PRODUCTIVE'},
    timestamp: { type: Date, default: Date.now },
    previous: { type: Schema.Types.Mixed, default: null }
});

const UserState = mongoose.model('UserState', UserStateSchema);
export {UserState}