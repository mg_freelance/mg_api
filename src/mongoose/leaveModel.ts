var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const LeaveRequestSchema = new Schema({
    leaveId: {type:String, default: ""},
    userId: {type:String, default: ""},
    startDate: {type:String, default: ""}, // TODO: THIS IS A DATE
    endDate: {type:String, default: ""}, // TODO: THIS IS A DATE
    reason: {type:String, default: ""},
    status: {type:String, default: ""}
});

const LeaveRequest = mongoose.model('LeaveRequest', LeaveRequestSchema);
export {LeaveRequest}