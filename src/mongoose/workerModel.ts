var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const WorkerSchema = new Schema({
    userId: String,
    firstName: String,
    lastName: String,
    siteEmail: String,
    departmentId: String
});

const Worker = mongoose.model('Worker', WorkerSchema);
export {Worker}