const moment = require('moment');
const Boom = require('boom');
import { Data } from '../../data/Data';
import { CalendarFilter } from './CalendarFilter';

export class CalendarRoles {

  public getFilterCalendarRoles(year: any, month: any, filterByUser: any, filterByDepartment: any, filterByRole: any, pattern: any) {
    if (filterByRole && filterByUser.toUpperCase() === "ALL" && filterByDepartment.toUpperCase() === "ALL") {
      return CalendarFilter.getCalendarRolesFilterByRoles(year, month, filterByRole, pattern);
    }
    if (filterByRole.toUpperCase() === "ALL" && filterByUser && filterByDepartment.toUpperCase() === "ALL") {
      return CalendarFilter.getCalendarRolesByUser(year, month, filterByUser, pattern);
    }
    if (filterByRole.toUpperCase() === "ALL" && filterByUser.toUpperCase() === "ALL" && filterByDepartment) {
      return CalendarFilter.getCalendarRolesFilterByDepartment(year, month, filterByDepartment, pattern);
    }

    if (filterByRole && filterByUser && filterByDepartment) {
      return CalendarFilter.getCalendarRolesUnfiltered(year, month, pattern);
    }

  }

  public getCalendarPattern() {
    return CalendarFilter.getCalendarPattern();
  }

  public setCalendarPattern(pattern: any) {
    CalendarFilter.setCalendarPattern(pattern);
  }

  public addNewRole(userId: string, roleId: string, fromDate: string, toDate: string, deptId: string) {
        if (userId === undefined || roleId === undefined || fromDate === undefined || toDate === undefined || deptId === undefined) {
            return (Boom.badRequest('Required values are missing.'));
        } else {
            CalendarFilter.addNewRole(userId, roleId, fromDate, toDate, deptId);
            return ({message: 'Added new Role successfully'});
        }
  }
}

export default new CalendarRoles();