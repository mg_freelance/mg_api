const moment = require('moment');
import { Data } from '../../data/Data';
import { CalendarPattern } from "../../mongoose/patternModel";
import { LoginUser } from "../../mongoose/userModel";

export class CalendarFilter {
    // public static calendarPattern = [];

    public static getCalendarRolesFilterByDepartment(year: any, month: any, filterByDepartment: any, pattern: any) {

        let rolesforMonth = [];
        //get number of days of the month
        let daysInMonth = moment(year + '-' + month, "YYYY-MM").daysInMonth();

        CalendarPattern.find({}).then((data) => {
            let x = data;
        });

        return LoginUser.find({}).then((users) => {
            return CalendarPattern.find({}).then((patterns) => {
                let calendarPattern = patterns;
                for (let i = 0; i < daysInMonth; i++) {
                    let date = year + '/' + (month as number) + '/' + (i + 1);
                    let weekNum = moment(date, "YYYY/MM/DD").week();

                    rolesforMonth.push({
                        year: year,
                        month: month,
                        dayOfMonth: i + 1,
                        weekOfYear: weekNum,
                        roles: []
                    });

                    for (let j = 0; j < calendarPattern.length; j++) {
                        for (let k = 0; k < calendarPattern[j].rolePattern.length; k++) {
                            if (moment(new Date(date)).isSame(new Date(calendarPattern[j].rolePattern[k].date), 'day')) {
                                let roleName = calendarPattern[j].rolePattern[k].role;
                                let roleId = calendarPattern[j].rolePattern[k].roleId;

                                const user = this.getUserById(users, calendarPattern[j].userId);
                                let obj = this.getRoleObj(user, calendarPattern, j, k);

                                if (calendarPattern[j].rolePattern[k].department.toUpperCase() === filterByDepartment.toUpperCase()) {
                                    rolesforMonth[i].roles.push(obj);
                                } else if ("ALL" === filterByDepartment.toUpperCase()) {
                                    rolesforMonth[i].roles.push(obj);
                                }

                            }
                        }
                    }
                }
                return this.cleanUpResponse(rolesforMonth);
            });
        });
    }


    private static cleanUpResponse(calendarRoles) {
        let temp = [];

        for (let i = 0; i < calendarRoles.length; i++) {
            let numberOfRoles = calendarRoles[i].roles.length;
            if (numberOfRoles > 0) {
                let rolesForDay = calendarRoles[i];
                let map = new Map();

                for (let s = 0; s < rolesForDay.roles.length; s++) {

                    let key = rolesForDay.roles[s].roleId + '-' + rolesForDay.roles[s].roleName;
                    let val = map.get(key);
                    if (!val) {
                        map.set(key, rolesForDay.roles[s].assignees);
                    } else {
                        let tmp = map.get(key);
                        tmp.push(rolesForDay.roles[s].assignees[0]);
                        map.set(key, tmp);
                    }
                }
                let roles = [];
                map.forEach(function (value, key) {
                    let keyFragments = key.split('-');
                    let tempRoleForDay = {
                        roleId: keyFragments[0],
                        roleName: keyFragments[1],
                        assignees: value
                    };
                    //rolesForDay.roles = [];
                    //rolesForDay.roles.push(tempRoleForDay);
                    roles.push(tempRoleForDay);
                });
                rolesForDay.roles = [];
                rolesForDay.roles = roles;
                roles = [];
                temp.push(rolesForDay);
            } else {
                temp.push(calendarRoles[i]);
            }
        }
        return temp;
    }


    //--- currently used method ------------------------
    public static getCalendarRolesFilterByRoles(year: any, month: any, filterByRole: any, pattern: any = false) {
        let rolesforMonth = [];
        //get number of days of the month
        let daysInMonth = moment(year + '-' + month, "YYYY-MM").daysInMonth();
        return LoginUser.find({}).then((users) => {
            return CalendarPattern.find({}).then((patterns) => {

                let calendarPattern = patterns;
                for (let i = 0; i < daysInMonth; i++) {
                    let date = year + '/' + (month as number) + '/' + (i + 1);
                    let weekNum = moment(date, "YYYY/MM/DD").week();

                    rolesforMonth.push({
                        year: year,
                        month: month,
                        dayOfMonth: i + 1,
                        weekOfYear: weekNum,
                        roles: []
                    });

                    for (let j = 0; j < calendarPattern.length; j++) {
                        for (let k = 0; k < calendarPattern[j].rolePattern.length; k++) {
                            if (moment(new Date(date)).isSame(new Date(calendarPattern[j].rolePattern[k].date), 'day')) {
                                let roleName = calendarPattern[j].rolePattern[k].role;
                                let roleId = calendarPattern[j].rolePattern[k].roleId;

                                const user = this.getUserById(users, calendarPattern[j].userId);
                                let obj = this.getRoleObj(user, calendarPattern, j, k);

                                if (roleId.toUpperCase() === filterByRole.toUpperCase()) {
                                    rolesforMonth[i].roles.push(obj);
                                } else if ("ALL" === filterByRole.toUpperCase()) {
                                    rolesforMonth[i].roles.push(obj);
                                }

                            }
                        }
                    }
                }
                return this.cleanUpResponse(rolesforMonth);
            });
        });
    }


    //--- currently used method ------------------------
    public static getCalendarRolesUnfiltered(year: any, month: any, pattern: any) {

        return LoginUser.find({}).then((users) => {
            return CalendarPattern.find({}).then((patterns) => {
                let calendarPattern = patterns;

                let rolesforMonth = [];
                //get number of days of the month
                let daysInMonth = moment(year + '-' + month, "YYYY-MM").daysInMonth();

                for (let i = 0; i < daysInMonth; i++) {
                    let date = year + '/' + (month as number) + '/' + (i + 1);
                    let weekNum = moment(date, "YYYY/MM/DD").week();

                    rolesforMonth.push({
                        year: year,
                        month: month,
                        dayOfMonth: i + 1,
                        weekOfYear: weekNum,
                        roles: []
                    });

                    for (let j = 0; j < calendarPattern.length; j++) {
                        for (let k = 0; k < calendarPattern[j].rolePattern.length; k++) {
                            if (moment(new Date(date)).isSame(new Date(calendarPattern[j].rolePattern[k].date), 'day')) {
                                let roleName = calendarPattern[j].rolePattern[k].role;
                                let roleId = calendarPattern[j].rolePattern[k].roleId;

                                const user = this.getUserById(users, calendarPattern[j].userId);
                                let obj = this.getRoleObj(user, calendarPattern, j, k);

                                rolesforMonth[i].roles.push(obj);

                            }
                        }
                    }
                }
                return this.cleanUpResponse(rolesforMonth);
            });
        });
    }



    //--- currently used method ------------------------
    public static getCalendarRolesByUser(year: any, month: any, filterByUser: any, pattern: any) {
        let rolesforMonth = [];
        //get number of days of the month
        let daysInMonth = moment(year + '-' + month, "YYYY-MM").daysInMonth();
        return LoginUser.find({}).then((users) => {
            return CalendarPattern.find({}).then((patterns) => {
                let calendarPattern = patterns;
                for (let i = 0; i < daysInMonth; i++) {
                    let date = year + '/' + (month as number) + '/' + (i + 1);
                    let weekNum = moment(date, "YYYY/MM/DD").week();

                    rolesforMonth.push({
                        year: year,
                        month: month,
                        dayOfMonth: i + 1,
                        weekOfYear: weekNum,
                        roles: []
                    });

                    for (let j = 0; j < calendarPattern.length; j++) {
                        for (let k = 0; k < calendarPattern[j].rolePattern.length; k++) {
                            if (moment(new Date(date)).isSame(new Date(calendarPattern[j].rolePattern[k].date), 'day')) {
                                let roleName = calendarPattern[j].rolePattern[k].role;
                                let roleId = calendarPattern[j].rolePattern[k].roleId;

                                const user = this.getUserById(users, calendarPattern[j].userId);
                                let obj = this.getRoleObj(user, calendarPattern, j, k);

                                if (user.userId.toUpperCase() === filterByUser.toUpperCase()) {
                                    rolesforMonth[i].roles.push(obj);
                                } else if ("ALL" === filterByUser.toUpperCase()) {
                                    rolesforMonth[i].roles.push(obj);
                                }

                            }
                        }
                    }
                }
                let response = this.cleanUpResponse(rolesforMonth);
                return response;
            });
        });
    }




    private static getUserById(users: any[], userId: any) {
        for (let i = 0; i < users.length; i++) {
            if (userId === users[i].userId) {
                return users[i];
            }
        }
    }

    private static getRoleObj(user: any, calendarPattern: any, j: any, k: any) {
        return {
            roleId: calendarPattern[j].rolePattern[k].roleId,
            roleName: calendarPattern[j].rolePattern[k].role,
            assignees: [{
                userId: user.userId,
                firstName: user.firstName,
                lastName: user.surName,
                siteEmail: user.siteEmail,
                departmentId: user.departmentId,
                shift: {
                    shiftStart: calendarPattern[j].rolePattern[k].startTime,
                    shiftEnd: calendarPattern[j].rolePattern[k].endTime
                }
            }]
        };
    }

    public static setCalendarPattern(pattern: any) {

        let startDate = pattern.startDate;
        let endDate = pattern.endDate;

        let startDayOfYear = moment(startDate, "YYYY/MM/DD");
        let endDayOfYear = moment(endDate, "YYYY/MM/DD");

        let daysMap = new Map();
        for (let j = 0; j < pattern.rolePattern.length; j++) {
            daysMap.set(pattern.rolePattern[j].day, pattern.rolePattern[j]);
        }

        let tempRolePattern = [];
        pattern.rolePattern = [];

        var duration = moment.duration(endDayOfYear.diff(startDayOfYear)).asDays();
        for (let i = 0; i <= duration; i++) {
            let dateToUse;
            if (i === 0) {
                dateToUse = moment(startDayOfYear.add(0, 'days')).format("YYYY/MM/DD");
            } else {
                dateToUse = moment(startDayOfYear.add(1, 'days')).format("YYYY/MM/DD");
            }
            let dayName = moment(dateToUse, 'YYYY/MM/DD').format('dddd').toUpperCase();

            let val = daysMap.get(dayName)
            if (val) {
                val['date'] = dateToUse;
                pattern.rolePattern.push({
                    day: val.day,
                    date: val.date,
                    roleId: val.roleId,
                    role: val.role,
                    department: val.department,
                    startTime: val.startTime,
                    endTime: val.endTime
                });
            }
        }

        CalendarPattern.find({}, (err, patterns) => {
            this.upsertPatterns(patterns, pattern)
        });
    }

    private static upsertPatterns(patterns: any[], pattern: any) {
        if (patterns.length === 0) {
            const _cp = new CalendarPattern(pattern);
            _cp.save((err) => {
                if (err) {
                    console.log('Adding user failed')
                } else {
                    // this.trackHistory("Logged In", request.headers["authorization"]);
                    console.log({ "status": 200, "message": "Successfully Created" });
                }
            });
        } else {
                for (const _cp of patterns) {
                let val = _cp.rolePattern;
                for (const _val of val) {
                    for (let _rolePattern of pattern.rolePattern) {
                        if ((_cp.userId === pattern.userId) && _val.date === _rolePattern.date) {
                            let __oldStartTime = Number(_val.startTime);
                            let __oldEndTime = Number(_val.endTime);
                            let __newStartTime = Number(_rolePattern.startTime);
                            let __newEndTime = Number(_rolePattern.endTime);

                            // eg: prev 8am - 10am, new 9am - 1pm
                            if ((__oldStartTime < __newStartTime) && (__newEndTime > __oldEndTime)) {
                                _val.endTime = _rolePattern.endTime;
                                _rolePattern = [];
                            }
                            // eg: prev 9am - 11am, new 8am - 10am
                            else if ((__oldStartTime > __newStartTime) && (__newEndTime < __oldEndTime)) {
                                _val.startTime = _rolePattern.startTime;
                                _rolePattern = [];
                            }
                            // eg: prev 9am - 11am, new 8am - 1pm
                            else if ((__oldStartTime > __newStartTime) && (__newEndTime > __oldEndTime)) {
                                _val.startTime = _rolePattern.startTime;
                                _val.endTime = _rolePattern.endTime;
                                _rolePattern = [];
                            } /*else if ((_val.startTime < _rolePattern.startTime) && (_rolePattern.endTime < _val.endTime)) {
                                // do nothing
                            } else {

                            }*/
                        }
                    }
                }
            }
            const _cp = new CalendarPattern(pattern);
            _cp.save((err) => {
                if (err) {
                    console.log('Adding pattern failed')
                } else {
                    // this.trackHistory("Logged In", request.headers["authorization"]);
                    console.log({ "status": 200, "message": "Successfully Created" });
                }
            });
        }
    }

    public static addNewRole(userId: string, roleId: string, startDate: string, endDate: string, deptId: string) {
        let startDayOfYear = moment(startDate, "YYYY/MM/DD");
        let endDayOfYear = moment(endDate, "YYYY/MM/DD");
        let _addedToExisitingUser = false;

        var duration = moment.duration(endDayOfYear.diff(startDayOfYear)).asDays();
        this.getCalendarPattern().then((patterns) => {
            for (const patternObj of patterns) {
            let __userId = patternObj.userId;
            if (__userId == userId) {
                for (let i = 0; i < duration; i++) {
                    let daysToAdd = (i === 0) ? 0 : 1;
                    patternObj.rolePattern.push({
                        day: startDayOfYear.add(i, 'day').format("dddd"),
                        date: startDayOfYear.add(i, 'day').format("YYYY/MM/DD"),
                        roleId: roleId,
                        role: this.getRoleByRoleId(roleId),
                        department: deptId,
                        startTime: '00.00',
                        endTime: '23.59'
                    });
                }
            } else {
                let _newPattern = <any>{};
                _newPattern.startDate = startDayOfYear;
                _newPattern.endDate = endDayOfYear;
                _newPattern.userId = userId;
                _newPattern.startDate = startDayOfYear;
                _newPattern.rolePattern = [];
                for (let i = 0; i < duration; i++) {
                    let daysToAdd = (i === 0) ? 0 : 1;
                    _newPattern.rolePattern.push({
                        day: startDayOfYear.add(i, 'day').format("dddd"),
                        date: startDayOfYear.add(i, 'day').format("YYYY/MM/DD"),
                        roleId: roleId,
                        role: this.getRoleByRoleId(roleId),
                        department: null,
                        startTime: '00.00',
                        endTime: '23.59'
                    });
                }

                new CalendarPattern(_newPattern).save((err) => {
                    if (err) {
                        console.log('Adding pattern failed')
                    } else {
                        console.log({ "status": 200, "message": "Successfully Created" });
                    }
                });
            }
        }
        });
    }

    public static getCalendarPattern() {
        return CalendarPattern.find({}).then(patterns => { return patterns });
    }

    private static getRoleByRoleId(id: string) {
        let rolename = '';
        Data.rolesToPick.map((_role) => {
            if (id == _role.roleId) {
                rolename = _role.roleName;
            }
        });
        return rolename;
    }
}

export default new CalendarFilter();