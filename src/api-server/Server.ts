import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
const moment = require('moment');
const fs = require('fs');
const path = require('path');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Blipp = require('blipp');
const BasicAuth = require('hapi-auth-basic')
const Joi = require('joi');
var pjson = require('../../package.json');
const Boom = require('boom');
const uuid = require('uuid');
var mongoose = require('mongoose');
import { Data } from '../data/Data';
import CalendarRoles from './calendar-roles/CalendarRoles';
import { SkillController } from '../models/skill/SkillController';
import { RoleController } from '../models/role/RoleController';
import { DepartmentController } from '../models/department/DepartmentController';
import { ChecklistController } from '../models/checklist/ChecklistController';
import { WorkflowController } from '../models/workflow/WorkflowController';
import { JobController } from '../models/job/JobController';
import { UploadController } from '../util/UploadController';
import { SprintController } from '../models/sprint/SprintController';
import { SetupListData } from '../data/jobsetup/SetupListData';
import {
    IEditLayoutOptions, IJobSetupFilterCriterias, SortPriority, SortOrder,
    WidthOptions, FieldOptions
} from "../data/jobsetup/JobSetupEditCriteriaSampleData";
import { LoginUser } from './../mongoose/userModel';
import { JobFieldsSetup } from './../mongoose/jobSetupModel';
import { LeaveRequest } from './../mongoose/leaveModel';
import { UserState } from './../mongoose/userStateModel';
import { JobSetupController } from "../models/job/JobSetupController";
import { ScheduleController } from  "../models/process/ScheduleController"
import { SequencingController } from "../models/process/SequncingController"
import { ReportingController } from "../models/reporting/ReportingController"
import { ListSetupController } from "../models/listsetup/ListSetupController";

export class APIServer {

    private server: Server = new Server;
    private nextUserId = 110;
    private nextRoleId = 20;
    private nextSkillId = 20;
    private nextLeaveId = 20;
    private nextCheckListId = 20;
    private nextDepartmentId = 20;
    private nextJobId = 20;
    private nextWorkFlowId = 20;

    private userKeys = ['firstName', 'surName', 'username',
        'siteEmail', 'roles', 'active',
        'temporaryStaff', 'contractStartDate', 'contractEndDate',
        'qualifications', 'skill', 'profileImg', 'permissionLevel'
    ];

    private historyTrack = [];

    // Data Collections
    // __________________________________
    private userList = Data.userList;
    private skillList = Data.skillList;
    private roleList = Data.roleList;
    private allocations = [];
    private leaves = Data.leaves;
    private departments = Data.departments;
    private checklists = Data.checklists;
    private workflows = Data.workflows;
    private jobSetupList = Data.jobSetupList;
    private jobList = Data.jobs;
    private sprintList = Data.sprints;
    private userStateList = Data.userStateList;
    // ____________________________________

    constructor() {
        mongoose.connect('mongodb://api.flowlogic.net:27017/flowLogicSite02', (err) => {
            if (err) console.log(err);
            else console.log('Connected to the database');
        });
    }



    private trackHistory(message: any, userAuthToken: any) {
        let user = this.findUserByAuthToken(userAuthToken);
        if (user != null) {
            this.historyTrack.push({
                userId: user.userId,
                userFirstName: user.firstName,
                userSurname: user.surName,
                history: message,
                timestamp: moment().format("ddd, MMM Do YYYY, h:mm:ss a")
            });
        }
    }

    private findUserByAuthToken(token: any) {
        for (var indx in this.userList) {
            if (this.userList[indx].authToken == token) {
                return this.userList[indx];
            }
        }
        return null;
    }

    public init() {
        var hash = hashSync("1234", 5);
        console.log(hash);
        this
            .server
            .connection({ port: 80 });

        const goodOptions = {
            ops: {
                interval: 1000
            },
            reporters: {
                myConsoleReporter: [
                    {
                        module: 'good-squeeze',
                        name: 'Squeeze',
                        args: [
                            {
                                log: '*',
                                response: '*'
                            }
                        ]
                    }, {
                        module: 'good-console'
                    },
                    'stdout'
                ]
            }
        };
        const options = {
            info: {
                'title': 'Flow-logic API Documentation',
                'version': pjson.version
            }
        };
        this
            .server
            .register([
                BasicAuth,
                Blipp,
                Inert, {
                    'register': require('good'),
                    'options': goodOptions
                },
                Vision, {
                    'register': HapiSwagger,
                    'options': options
                }
            ], () => { });
        this
            .server
            .auth
            .strategy('simple', 'basic', { validateFunc: this.validate });
        this
            .server
            .auth
            .default('simple');

        // _________________________________________________________________________
        var upload = new UploadController(this.server, this.trackHistory);
        for (var elem in upload.getRouteList()) {
            this.server.route(upload.getRouteList()[elem]);
        }

        var skillC = new SkillController(this.server, this.trackHistory, this.nextSkillId);
        for (var elem in skillC.getRouteList()) {
            this.server.route(skillC.getRouteList()[elem]);
        }

        var roleC = new RoleController(this.server, this.trackHistory, this.nextRoleId);
        for (var elem in roleC.getRouteList()) {
            this.server.route(roleC.getRouteList()[elem]);
        }

        var departmentC = new DepartmentController(this.server, this.trackHistory, this.nextDepartmentId);
        for (var elem in departmentC.getRouteList()) {
            this.server.route(departmentC.getRouteList()[elem]);
        }

        var checklistC = new ChecklistController(this.server, this.trackHistory, this.nextCheckListId);
        for (var elem in checklistC.getRouteList()) {
            this.server.route(checklistC.getRouteList()[elem]);
        }

        var workflowC = new WorkflowController(this.server, this.trackHistory, this.nextWorkFlowId);
        for (var elem in workflowC.getRouteList()) {
            this.server.route(workflowC.getRouteList()[elem]);
        }

        var jobC = new JobController(this.server, this.trackHistory, this.nextJobId);
        for (var elem in jobC.getRouteList()) {
            this.server.route(jobC.getRouteList()[elem]);
        }


        var SprintC = new SprintController(this.server, this.trackHistory);
        for (var elem in SprintC.getRouteList()) {
            this.server.route(SprintC.getRouteList()[elem]);
        }

        var jobSC = new JobSetupController(this.server, this.trackHistory);
        for (var elem in jobSC.getRouteList()) {
            this.server.route(jobSC.getRouteList()[elem]);
        }

        var reporting = new ReportingController(this.server, this.trackHistory);
         for (var elem in reporting.getRouteList()) {
            this.server.route(reporting.getRouteList()[elem]);
        }

        var listSetup = new ListSetupController();
        for (var elem in listSetup.getRouteList()) {
            this.server.route(listSetup.getRouteList()[elem]);
        }
        // _________________________________________________________________________

        this
            .server
            .route({
                method: "GET",
                path: "/",
                config: {
                    auth: false,
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    reply("Flow Logic API Serving " + pjson.version + " ....");
                }

            });

        this
            .server
            .route({
                method: "POST",
                path: "/login",
                config: {
                    auth: false,
                    description: "Endpoint used for login and receiving the authentication token",
                    notes: "This endpoint should be called to authenticate a user and receive the authentica" +
                    "tion token",
                    tags: [
                        'api', 'v1'
                    ],
                    validate: {
                        payload: Joi.object({
                            username: Joi
                                .string()
                                .max(30)
                                .required(),
                            password: Joi
                                .string()
                                .max(30)
                                .required(),
                            siteEmail: Joi
                                .string()
                                .max(30)
                                .required()
                        }),
                        failAction: (request, reply, source, error) => {
                            reply(Boom.badRequest('Invalid Payload, username,password and site email expected'));
                        }
                    },
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    const username = request.payload.username.toLowerCase();
                    const password = request.payload.password;

                    if (!username || !password) {
                        reply(Boom.unauthorized('Invalid username or PIN'));
                        return;
                    }

                    LoginUser.find({username: username}, (err, users) => {
                        if(err || users.length===0){
                            reply(Boom.notFound('Invalid username'));
                            return;
                        }

                        const _isValidPassword = compareSync(password, users[0].password);
                        if(_isValidPassword){
                            const _user = users[0];
                            var user = {
                                name: _user.username,
                                userId: _user.userId,
                                profileImg: _user.profileImg,
                                authToken: _user.authToken,
                                permissionLevel: _user.permissionLevel
                            };
                            this.trackHistory("Logged In", _user.authToken);
                            reply(user)
                        }else{
                            reply(Boom.unauthorized('Invalid username, or password'));
                            return;
                        }
                    })
                }
            });

        this
            .server
            .route({
                method: "GET",
                path: "/user",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve the list of users",
                    notes: "Endpoint used to retrieve the list of users",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    //LoginUser.find({isDeleted: false}, (err, users) => {
                    LoginUser.find({}, (err, users) => {
                        reply(users)
                    });
                }

            });

        this
            .server
            .route({
                method: "GET",
                path: "/user/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve a user from id",
                    notes: "Endpoint used to retrieve a user",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    const userId = request.params["userId"];
                    LoginUser.findOne({userId: userId}, (err, users) => {
                        if(err) reply(Boom.notFound('Couldnt find the user'));
                        else reply(users)
                    });
                }

            });

        this
            .server
            .route({
                method: "POST",
                path: "/user",
                config: {
                    auth: false,
                    description: "Endpoint used add a new user",
                    notes: "Endpoint used to add a new user",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {

                    let user = request.payload;
                    if(user._id){
                        delete user._id;
                    }
                    user.password = hashSync(user.password, 5);
                    user.userId = '' + (this.nextUserId++);

                    /**
                     * UNCOMMENT AFTER ADDING NECESSARY FIELDS TO THE DATAMODEL
                     *
                     *  user.createdBy = request.headers['userAuthorized']
                        user.createdTimeStamp = new Date().toISOString()
                        user.isDeleted = false

                     *
                     */

                    const _loginUser = new LoginUser(user);
                    _loginUser.save( err =>{
                        if(err) reply(Boom.unauthorized('Adding user failed'));
                        else {
                            // this.trackHistory("Logged In", request.headers["authorization"]);
                            reply({ "status": 200, "message": "Successfully Created", user: _loginUser });
                        }
                    });
                }

            });

        this
            .server
            .route({
                method: "PUT",
                path: "/user/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint used to update user",
                    notes: "Endpoint used to update user",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    const userId = request.params["userId"];
                    const newuser = request.payload;

                    LoginUser.findOne({userId: userId}, (err, user)=>{
                       if(err || !user) reply(Boom.unauthorized('Couldnt find the user'));
                       for (let key in user){
                           if(!(key==='password')){
                                user[key] = newuser[key] ? newuser[key] : user[key]
                           }
                       }

                       const _isSamePassowrd = compareSync(newuser.password, user.password);
                       if(!_isSamePassowrd){
                           user.password = hashSync(newuser.password, 5);
                       }

                    /**
                     * UNCOMMENT AFTER ADDING NECESSARY FIELDS TO THE DATAMODEL
                     *
                     *  user.modifiedBy = request.headers['userAuthorized']
                        user.modifiedTimeStamp = new Date().toISOString()

                     *
                     */

                       user.save( (err, user) => {
                            if(err) reply(Boom.unauthorized('update user failed'));
                            else reply({ "status": 201, "message": "Successfully Updated", user: user })
                       })

                    })
                }

            });


        this
            .server
            .route({
                method: "DELETE",
                path: "/user/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint to Delete a users",
                    notes: "Endpoint to Delete a users",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    const userId = request.params["userId"];

                    /**
                     * REPLACE AFTER ADDING NECESSARY FIELDS TO THE DATAMODEL
                     *
                     *
                        LoginUser.findOne({userId: userId}, (err, user)=>{
                            if(err || !user) reply(Boom.unauthorized('Couldnt find the user'));

                                user.deletedBy = request.headers['userAuthorized']
                                user.deletedTimeStamp = new Date().toISOString()
                                user.isDeleted = true



                            user.save( (err, user) => {
                                    if(err) reply(Boom.unauthorized('delete user failed'));
                                    else reply({ "status": 200, "message": "Successfully Deleted"})
                            })


                        })
                    *
                    */
                    LoginUser.remove({userId: userId},(err) =>{
                        if(err) reply(Boom.unauthorized('delete user failed'));
                        else reply({ "status": 200, "message": "User Deleted"})
                    })
                }
            });


        this
            .server
            .route({
                method: "GET",
                path: "/jobSetup/{jobSetupId}",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve job setup template",
                    notes: "Endpoint used to retrieve job setup template",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {
                    const jobSetupId = request.params["jobSetupId"];

                    JobFieldsSetup.findOne({jobSetupId: jobSetupId}, (err, jobSetup) => {
                        if(err) reply(Boom.notFound('Couldnt find the jobSetup'));
                        else reply(jobSetup)
                    });
                }
            });

        this
            .server
            .route({
                method: "PUT",
                path: "/jobSetup/{jobSetupId}",
                config: {
                    auth: false,
                    description: "Endpoint used to update job setup",
                    notes: "Endpoint used to update job setup",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    let newJobSetup = request.payload;
                    const jobSetupId = request.params["jobSetupId"];
                    if(newJobSetup._id){
                        delete newJobSetup._id;
                    }

                    JobFieldsSetup.findOne({jobSetupId: jobSetupId}, (err, jobSetup)=>{
                        if(err) reply(Boom.notFound('Couldnt find the jobSetup'));
                        else {

                          for(let key in jobSetup){
                              jobSetup[key] = newJobSetup[key] ? newJobSetup[key] : jobSetup[key];
                          }

                          jobSetup.save(err => {
                              if(err) reply(Boom.unauthorized('Updating Job setup failed'));
                              else {
                                  reply({ "status": 200, "message": "Successfully Updated", jobSetup: jobSetup });
                              }
                          })
                        }
                    })

                }
            });

        this
            .server
            .route({
                method: "GET",
                path: "/history",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve the history list",
                    notes: "Endpoint used to retrieve the history list",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    reply(this.historyTrack);
                }

            });

        this
            .server
            .route({
                method: "GET",
                path: "/calendar/leaves",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve the list of leaves",
                    notes: "Endpoint used to retrieve the list of leaves",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    LeaveRequest.find({}, (err, leaves) => {
                        if(err) reply(Boom.notFound('Couldnt find Leaves'));
                        else reply(leaves)
                    })
                }

            });


        this
            .server
            .route({
                method: "GET",
                path: "/public/{param*}",
                config: {
                    auth: false,
                    description: "Endpoint to server static files in public folder",
                    notes: "Endpoint used to retrieve the history list",
                    tags: [
                        'api', 'v1'
                    ],
                },
                handler: {
                    directory: {
                        path: path.normalize(__dirname + '/public')
                    }
                }

            });


        this
            .server
            .route({
                method: "GET",
                path: "/calendar/leave/summary",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve the list of leaves",
                    notes: "Endpoint used to retrieve the list of leaves",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    LeaveRequest.find({}, (err, leaves) => {
                        if(err) reply(Boom.notFound('Couldnt find Leaves'));
                        else reply(leaves)
                    })
                }

            });

        this
            .server
            .route({
                method: "GET",
                path: "/calendar/leave",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve the list of leaves",
                    notes: "Endpoint used to retrieve the list of leaves",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {

                    let query:any = {};
                    const userId = request.query.userId;
                    const startDate = request.query.startDate;

                    if(userId && startDate){
                        query.userId = userId;
                        query.startDate = startDate;
                    }else if(userId) {
                        query.userId = userId;
                    } else if(startDate){
                        query.startDate = startDate;
                    }

                    LeaveRequest.find(query, (err, leaves) => {
                        if(err) reply(Boom.notFound('Couldnt find Leaves'));
                        else reply(leaves)
                    })
                }

            });

        this
            .server
            .route({
                method: "POST",
                path: "/calendar/leave",
                config: {
                    auth: false,
                    description: "Endpoint used to request leave",
                    notes: "Endpoint used to request leave",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    let newLeave = request.payload;
                    newLeave.leaveId = 'L' + request.payload.userId + (new Date().getTime());
                    if(newLeave._id){
                        delete newLeave._id
                    }
                    var _leave = new LeaveRequest(request.payload)
                    _leave.save(err => {
                                if(err) reply(Boom.unauthorized('Couldnt add leave'));
                                else {
                                    reply({ "status": 200, "message": "Successfully added", leave: _leave });
                                }
                            })


                    // this.trackHistory("Added Leave Request", request.headers["authorization"]);
                    // reply({ "status": 200, "message": "Successfully Updated" });
                }
            });

        this
            .server
            .route({
                method: "PUT",
                path: "/calendar/leave/{leaveId}",
                config: {
                    auth: false,
                    description: "Endpoint used to update a leave request",
                    notes: "Endpoint used to update a leave request",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    const leaveId = request.params["leaveId"];
                    const newLeave = request.payload;

                    if(newLeave && newLeave._id){
                        delete newLeave._id
                    }
                    
                    LeaveRequest.findOne({leaveId: leaveId}, (err, leave) => {
                        if(err) {
                            reply(Boom.notFound('Couldnt find leave'));
                        } else if (newLeave) {
                            leave.reason = newLeave.comment;
                            leave.status = newLeave.status;

                            leave.save(err => {
                                if(err) {
                                    reply(Boom.unauthorized('Updating leave failed'));
                                } else { 
                                    reply({ "status": 200, "message": "Successfully Updated", leaveId: leaveId });
                                }
                            })
                        }
                    })                    
                }
            });

        this
            .server
            .route({
                method: "GET",
                path: "/calendar/roles/{year}/{month}/",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve the list of roles with assignees for the day",
                    notes: "Endpoint used to retrieve the list of roles with assignees for the day",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    let year = request.paramsArray[0];
                    let month = request.paramsArray[1];
                    let filterByUser = request.query.user;
                    let filterByDepartment = request.query.department;
                    let filterByRole = request.query.role;
                    let pattern = request.query.pattern;

                    if (filterByUser === undefined && filterByDepartment === undefined && filterByRole === undefined) {
                        reply(Boom.badRequest('Required values are missing.'));

                    }
                    CalendarRoles.getFilterCalendarRoles(year, month, filterByUser, filterByDepartment, filterByRole, pattern).then((data) => {
                        reply(data);
                    })
                }

            });

            this
            .server
            .route({
            method: "POST",
            path: "/calendar/role",
            config: {
                auth: false,
                description: "Endpoint used to set job role pattern",
                notes: "Endpoint used to set job role pattern",
                tags: [
                    'api', 'v1'
                ],
                plugins: {
                    'hapi-swagger': {
                        responses: {
                            '200': {
                                'description': 'Success'
                            }
                        }
                    }
                }
            },
            handler: function (request, reply) {
                let userId = request.payload.userId;
                let roleId = request.payload.roleId;
                let startDate = request.payload.startDate;
                let endDate = request.payload.endDate;
                let departmentId = request.payload.departmentId;
                reply(CalendarRoles.addNewRole(userId, roleId, startDate, endDate, departmentId));
            }
        });

        this
            .server
            .route({
                method: "POST",
                path: "/calendar/pattern",
                config: {
                    auth: false,
                    description: "Endpoint used to set job role pattern",
                    notes: "Endpoint used to set job role pattern",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {


                    if (!request.payload) {
                        reply(Boom.badRequest('Required values are missing.'));

                    }
                    CalendarRoles.setCalendarPattern(request.payload);
                    reply({ "status": 200, "message": "Successfully Updated" });

                }

            });

        this
            .server
            .route({
                method: "GET",
                path: "/calendar/pattern",
                config: {
                    auth: false,
                    description: "Endpoint used to get job role pattern",
                    notes: "Endpoint used to get job role pattern",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    reply(CalendarRoles.getCalendarPattern());
                }

            });

        // -------------------- START SETUP LISTS -------------------------------------------------------------
        this
            .server
            .route({
                method: "GET",
                path: "/setup/lists",
                config: {
                    auth: false,
                    description: "Endpoint used to get all lists available in setup",
                    notes: "Endpoint used to get all lists",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    reply(SetupListData.getAllLists());
                }

            });

        this
            .server
            .route({
                method: "GET",
                path: "/setup/list/{listid}",
                config: {
                    auth: false,
                    description: "Endpoint used to get all lists available in setup",
                    notes: "Endpoint used to get all lists",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    let listId = request.paramsArray[0];
                    reply(SetupListData.getListById(listId));
                }

            });

        // -------------------- END SETUP LISTS ---------------------------------------------------------------

        // -------------------- START LOGIC METHODS ---------------------------------------------------------------
        this
            .server
            .route({
                method: "POST",
                path: "/TriggerSequenceLogic",
                config: {
                    auth: false,
                    description: "Endpoint used to trigger the sequence logic",
                    notes: "Endpoint used to trigger the sequence logic",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {
                    let filteredJobList = null
                    if(request.payload != null && request.payload['filtered'] != null && request.payload['filtered'][0] != null ){
                        filteredJobList = request.payload['filtered'].map((job)=>{
                            return job.jobID
                        })
                    }

                     let controller: SequencingController = new SequencingController()
                     controller.TriggerSequenceLogic(filteredJobList != null? filteredJobList: null)
                reply({ "status": 200, "message": "Successfully Triggered" });

                }
            });

        this
            .server
            .route({
                method: "GET",
                path: "/process/scheduleStartDate",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve a recomended scheduled start date for jobs",
                    notes: "Endpoint used to retrieve a recomended scheduled start date for jobs",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {

                    let data: ScheduleController = new ScheduleController()

                    reply(data.getRecommendedStartDate(''))
                }
            });

          // -------------------- END LOGIC METHODS ---------------------------------------------------------------

          // -------------------- USER ALLOCATION ENDPOINTS -------------------------------------------------------------------

        this
            .server
            .route({
                method: "GET",
                path: "/jobAllocation/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve job Allocations for a user",
                    notes: "Endpoint used to retrieve job Allocations for a user",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {

                    let data: SequencingController = new SequencingController()

                    reply(data.getUserCurrentNextJobs(request.paramsArray[0]))
                }
            });

        this
            .server
            .route({
                method: "POST",
                path: "/triggerJobStart/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint used to trigger a start of an assigned job",
                    notes: "Endpoint used to trigger a start of an assigned job",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {

                    let data: SequencingController = new SequencingController()
                    data.startAssignedJob(request.paramsArray[0], request.payload.timeStamp)
                    reply({ "status": 200, "message": "Successfully Triggered" });
                }
            });


        this
            .server
            .route({
                method: "POST",
                path: "/triggerJobComplete/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint used to trigger an end of an assigned job",
                    notes: "Endpoint used to trigger an end of an assigned job",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {

                    let data: SequencingController = new SequencingController()
                    data.setCompleteAssignedJob(request.paramsArray[0], request.payload.timeStamp)
                    reply({ "status": 200, "message": "Successfully Triggered" });
                }
            });

          // -------------------- END USER ALLOCATION ENDPOINTS ---------------------------------------------------------------

        this
            .server
            .route({
                method: "POST",
                path: "/reports/jobs",
                config: {
                    auth: false,
                    description: "Endpoint used to send reports for job",
                    notes: "Endpoint used to send reports for job",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }

                },
                handler: (request: Request, reply: IReply) => {

                    reply({ "status": 200, data: request.payload });

                }

            });


        this
            .server
            .route({
                method: "GET",
                path: "/userState/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve user state",
                    notes: "Endpoint used to retrieve user state",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {
                    const userId = request.params["userId"];
                    UserState.find({userId: userId}, (err, userState) => {
                        if(err) reply(Boom.notFound('Couldnt find the user'));
                        else reply(userState)
                    })
                }
            });


        this
            .server
            .route({
                method: "PUT",
                path: "/userState/{userId}",
                config: {
                    auth: false,
                    description: "Endpoint to change user state",
                    notes: "Endpoint change user state",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {

                    const userId = request.params["userId"];
                    let newState = request.payload;

                    if(newState._id){
                        delete newState._id
                    }

                    if(newState.previous){
                        delete newState.previous
                    }

                    if(newState.timestamp){
                        delete newState.timestamp
                    }

                    UserState.findOne({ userId: userId }, (err, currentState) => {
                        if(err) reply(Boom.notFound('Couldnt find the user'));

                        if(currentState){ // have a user
                            const _previousState = {
                                calendaredOff: currentState.calendaredOff,
                                onBreak: currentState.onBreak,
                                state: currentState.state,
                                timestamp: currentState.timestamp
                            };

                            currentState.previous = _previousState;
                            currentState.timestamp = new Date().toISOString();

                            for(let key in newState){
                                currentState[key] = newState[key]
                            }

                            currentState.save(err =>{
                                if(err) reply(Boom.notFound('Couldnt find the user'));
                                else reply({status: 201, message: 'State Updated', currentState})
                            })

                        }else{
                            let state = new UserState(newState);
                            state.timestamp = new Date().toISOString();
                            state.userId = userId;

                            state.save(err => {
                                if(err) reply(Boom.notFound('Couldnt find the user'));
                                else reply({status: 201, message: 'State Updated', currentState})
                            })

                        }

                    })

                }
            });

        this
            .server
            .route({
                method: "GET",
                path: "/process/getScheduledStartDate",
                config: {
                    auth: false,
                    description: "Endpoint used to retrieve a recomended scheduled start date for jobs",
                    notes: "Endpoint used to retrieve a recomended scheduled start date for jobs",
                    tags: [
                        'api', 'v1'
                    ],
                    plugins: {
                        'hapi-swagger': {
                            responses: {
                                '200': {
                                    'description': 'Success'
                                }
                            }
                        }
                    }
                },
                handler: (request: Request, reply: IReply) => {

                    let data: ScheduleController = new ScheduleController()

                    reply(data.getRecommendedStartDate(request.params["userId"]))



                    // if (request.params["userId"] !== null) {

                    //     var filtered = this.userStateList.filter((state) => {
                    //         return state.userId.toString() === request.params["userId"];
                    //     });

                    //     if (filtered.length === 0) {
                    //         reply(Boom.notFound('userId not found'));
                    //     } else {
                    //         reply(filtered[0]);
                    //     }
                    // } else {
                    //     reply(Boom.notFound("userId is missing"));
                    // }
                }
            });


        return this.server;

    }



    /**
     *
     */
    private validate(request: Request, username: string, password: string, callback: Function): void {
        console.log("Authenticating " + username);
        callback(null, true, {
            id: 1,
            name: 'Lasitha'
        });
    }



}
