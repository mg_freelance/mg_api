/**
 * Copyright (c) All Rights Reserved ByteBack Limited
 *
 * @author Lasitha Petthawadu
 */
import { Server, Request, IReply } from 'hapi';
import { hashSync, compareSync } from 'bcrypt';
import { Data } from '../data/Data';

const path = require('path');
const fs = require('fs');
const uuid = require('uuid');

export class UploadController {
    private _server: Server;

    constructor(server: Server, trackHistory: any) {

        this._server = server;
    }

    private upload = {
        method: 'POST',
        path: '/upload',
        config: {
            auth: false,
            payload: {
                output: 'stream',
                parse: true,
                maxBytes: 209715200,
            },

            handler: function (request: Request, reply: IReply) {

                var data = request.payload[Object.keys(request.payload)[0]];
                var extension = path.extname(data.hapi.filename);
                var filename = uuid.v4() + extension;


                const _dir =  __dirname + "/../api-server/public/";
                if (!fs.existsSync(_dir)) {
                    fs.mkdirSync(_dir);
                }

                var pathLocation = _dir + filename;
                var file = data.pipe(fs.createWriteStream(pathLocation));

                file.on('finish', function (err) {
                    if (err) {
                        reply({ error: err }).code(500);
                    }
                    var res = {
                        originalName: data.hapi.filename,
                        headers: data.hapi.headers,
                        path: pathLocation,
                        fileName: filename
                    }
                    reply({ res }).code(201);
                })
            }
        }

    };

    public getRouteList(): any[] {
        return [this.upload];
    }
}