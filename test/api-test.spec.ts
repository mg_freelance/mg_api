import {assert} from 'chai';
import {APIServer} from '../src/api-server/Server'
const server:APIServer = new APIServer();

describe("API Tests", ()=>{
    it("Should Check if Base Returns version",(done)=>{
        let s=server.init();
        s.inject({ method: 'GET', url: '/'}, (res) => {
            assert.equal(200, res.statusCode);
            assert.isTrue(res.payload.indexOf("1.0.0")>0);
            done();
        });
    });
}

)